//
//  LayerLockSprite.cpp
//  Bra
//
//  Created by PR1.Stigol on 01.03.13.
//
//
#include "LayerLockSprite.h"
#include "Scene\BGameScene.h"
#include "SimpleAudioEngine.h"
#include "Component\BElementClip.h"

int LayerLockSprite::count_object = 0;
#pragma mark  - LayerLockSprite
LayerLockSprite::~LayerLockSprite()
{
    LayerLockSprite::count_object--;
    CCLOG("LayerLockSprite::destroy count %d",LayerLockSprite::count_object);
    m_lock->unloadAtlasTexture();
    CC_SAFE_DELETE(m_bracloth);
    
//    m_bracloth->center->release();
//    m_bracloth->left->release();
//    m_bracloth->right->release();
//    m_bracloth = NULL;
    
    removeAllChildrenWithCleanup(true);
    CC_SAFE_RELEASE_NULL(m_lock);
};
LayerLockSprite::LayerLockSprite()
{
    LayerLockSprite::count_object++;
    CCLOG("LayerLockSprite::LayerLockSprite count %d",LayerLockSprite::count_object);
    start_end_animation = true;
    setTouchEnabled(false);
    is_ipad = (win_size.width == 1024);
    //element = NULL;
    m_lock = NULL;
    slide_off_broken =0;
    istouch = false;
    for (int i = 0; i < 5; ++i) {
        mouse_touch[i].element = NULL;
        mouse_touch[i].is_active = false;
    };
    count_touch = 0;
};

BaseLayerLock * LayerLockSprite::create()
{
    BaseLayerLock *pRet = new LayerLockSprite();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        CC_SAFE_DELETE(pRet);
        return NULL;
    }
};

void LayerLockSprite::draw()
{
};

void LayerLockSprite::runSlap()
{
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("BraSlap.caf");
};
void LayerLockSprite::runTouchEnd(const int& _touchId)
{
if(mouse_touch[_touchId].is_active && mouse_touch[_touchId].element)
{
    mouse_touch[_touchId].element->endTouch();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("ButtonInLucky.mp3");
    
    
    if(mouse_touch[_touchId].element->getFuncName() == "runAction")
    {
        if(*mouse_touch[_touchId].element->getValue() == 6) //
        {
            m_lock->getChildByTag(16)->runAction(CCSequence::create(CCRotateTo::create(0.3, -150),CCRotateTo::create(0.05, -130),CCRotateTo::create(0.05, -135),NULL));
            m_lock->getChildByTag(15)->runAction(CCSequence::create(CCRotateTo::create(0.3, 150),CCRotateTo::create(0.05, 130),CCRotateTo::create(0.05, 135),NULL));
            NodeToClip(m_lock->getChildByTag(3))->setPathFilter(fNone);
            NodeToClip(m_lock->getChildByTag(4))->setPathFilter(fNone);
        };
        if(*mouse_touch[_touchId].element->getValue() == 8) //smile
        {
            mouse_touch[_touchId].element->setEnabled(false);
            state_lock++;
            if(NodeToClip(m_lock->getChildByTag(3))->getPathProgress() > 0.9 && state_lock == 2)
            {
                startEndAnimation();
            };
        };
        if(*mouse_touch[_touchId].element->getValue() == 9)//in-ayn
        {
            if(mouse_touch[_touchId].element->getTag() == 11 && mouse_touch[_touchId].element->getRotation() < -20) // left
            {
                mouse_touch[_touchId].element->runAction(CCSequence::create(CCRotateTo::create(0.15, -100),CCRotateTo::create(0.05, -85),CCRotateTo::create(0.03, -90),NULL));
            };
            
            if(mouse_touch[_touchId].element->getTag() == 12 && mouse_touch[_touchId].element->getRotation() < 160) // right
            {
                mouse_touch[_touchId].element->runAction(CCSequence::create(CCRotateTo::create(0.15, 100),CCRotateTo::create(0.05, 85),CCRotateTo::create(0.03, 90),NULL));
            };
            if(m_lock->getChildByTag(12)->getRotation() < 160 && m_lock->getChildByTag(11)->getRotation() < -20)
            {
                NodeToClip(m_lock->getChildByTag(3))->setEnabled(true);
            }
        };
        
        if(*mouse_touch[_touchId].element->getValue() == 3) //red lock
        {
            if(NodeToClip(m_lock->getChildByTag(3))->getPathProgress() > 0.4 && NodeToClip(m_lock->getChildByTag(4))->getPathProgress() > 0.4)
            {
                startEndAnimation();
            }
        }
    };
    mouse_touch[_touchId].element = NULL;
    count_touch--;
};
};

void LayerLockSprite::ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    CCSetIterator it;
    CCTouch* touch;
    if(start_end_animation)
        return;
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        // CCLOG("ccTouchesBegan:: %d",touch->getID());
        if(!touch)
            break;
        runTouchEnd(touch->getID());
    };
    count_length_null = 0;
    istouch = false;
    //return lock to start position
    if(count_touch == 0)
    {
        CCPoint point = ccpSub(ccp(getContentSize().width*lock_start_position.x,getContentSize().height*lock_start_position.y), m_lock->getPosition());
        
        CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(slide_off_broken);
        slide_off_broken = 0;
      //  CCLOG("ccpLength %f",ccpLength(point));
        if(ccpLength(point) > 150)
        {
            this->runAction(CCSequence::create(CCDelayTime::create(0.05),CCCallFunc::create(this, callfunc_selector(LayerLockSprite::runSlap)),NULL));
        }
        m_lock->runAction(CCMoveTo::create(0.05, ccp(getContentSize().width*lock_start_position.x,getContentSize().height*lock_start_position.y))) ;
    };
};

void LayerLockSprite::ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    CCSetIterator it;
    CCTouch* touch;
    if(start_end_animation)
        return;
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        // CCLOG("ccTouchesBegan:: %d",touch->getID());
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        CCLOG("LayerLockSprite::ccTouchesBegan");
        BElement* element = m_lock->getTouchElement(location,BRA::elBUTTON | BRA::elCLIPPER);
        if(element != NULL)
        {
            mouse_touch[touch->getID()].element = static_cast<BElementButton *>(element) ;
            element->setState(1);
            mouse_touch[touch->getID()].is_active = true;
            CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("ButtonPressed.wav");
            static_cast<BElementClip *>(element)->startEnd();
            
            istouch = true;
            element->beginTouch();
            element->setTouchPosition(location);
            count_touch++;
            if(count_touch == 1)
            {
                base_point = location;
            };
        }
        else
        {
            istouch = false;
        };

    };
};

void LayerLockSprite::ccTouchesMoved(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    CCSetIterator it;
    CCTouch* touch;
    if(start_end_animation)
        return;
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
      //   CCLOG("ccTouchesMoved:: %d",touch->getID());
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        if(mouse_touch[touch->getID()].is_active && mouse_touch[touch->getID()].element)
        {
            if(count_touch == 1)
            {
                CCPoint dir = ccpSub(location, base_point);
                base_point = location;
                //CCLOG("mouse_touch[touch->getID()].element->getType() %d",mouse_touch[touch->getID()].element->getType());
                
                CCPoint point = ccpSub(ccp(getContentSize().width*lock_start_position.x,getContentSize().height*lock_start_position.y), m_lock->getPosition());
              //  CCLOG("ccpLength %f",ccpLength(point));
                if(ccpLength(point) > 150 )
                {
                    if(slide_off_broken == 0)
                        slide_off_broken = CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("SlideOffBorders.caf");
                }
                else
                {
                    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(slide_off_broken);
                    slide_off_broken = 0;
                }
                
                if (mouse_touch[touch->getID()].element->getType() != BRA::elBUTTON ) //move mono lock
                {
                    count_length_null = 0;
                    m_lock->setPosition(ccpAdd(m_lock->getPosition(), dir));
                }
                else
                {
                   // CCPoint prev_pos = mouse_touch[touch->getID()].element->getPosition();
                    if(!mouse_touch[touch->getID()].element->moveTo(location))
                    {
                        m_lock->setPosition(ccpAdd(m_lock->getPosition(), dir));
                    }else if( !mouse_touch[touch->getID()].element->isPointInObject(mouse_touch[touch->getID()].element->getParent()->convertToNodeSpace(location)))
                    {
                        m_lock->setPosition(ccpAdd(m_lock->getPosition(), dir));
                    }
                    else
                    {

//                        float length = ccpLength(ccpSub(mouse_touch[touch->getID()].element->getPosition(), prev_pos));
//                        CCLOG("length %f",length);
//                        if(length == 0.0)
//                        {
//                            count_length_null++;
//                        }
//                        else
//                        {
//                            count_length_null =0;
//                        }
//                        if(count_length_null > 7)
//                        {
//                            m_lock->setPosition(ccpAdd(m_lock->getPosition(), dir));
//                        }
                        
                    }
                }
            }
            else
            {
                count_length_null = 0;
                if (mouse_touch[touch->getID()].element->getType() == BRA::elBUTTON) {
                    mouse_touch[touch->getID()].element->moveTo(location);
                }
            }
        }
    };
    
    if(m_lock->getType() == 1)
    {
        if(NodeToClip(m_lock->getChildByTag(3))->getPathProgress() > 0.94 && NodeToClip(m_lock->getChildByTag(4))->getPathProgress() > 0.94)
        {
            startEndAnimation();
        }
    }
    else if(m_lock->getType() == 2)
    {
        if(NodeToClip(m_lock->getChildByTag(3))->getPathProgress() > 0.94 && NodeToClip(m_lock->getChildByTag(4))->getPathProgress() > 0.94)
        {
            startEndAnimation();
        }
    }
        else if(m_lock->getType() == 3)
    {
        if(NodeToClip(m_lock->getChildByTag(3))->getPathProgress() > 0.4 && NodeToClip(m_lock->getChildByTag(4))->getPathProgress() > 0.4)
        {
            NodeToClip(m_lock->getChildByTag(11))->setEnabled(true);
        }
        else
        {
            NodeToClip(m_lock->getChildByTag(11))->setEnabled(false);
        };
    }
    else if(m_lock->getType() == 4)
    {
        if(NodeToClip(m_lock->getChildByTag(3))->getPathProgress() > 0.98)
        {
            startEndAnimation();
        };
    }
    else if(m_lock->getType() == 5)
    {
        if(NodeToClip(m_lock->getChildByTag(3))->getPathProgress() + NodeToClip(m_lock->getChildByTag(4))->getPathProgress() > 0.98)
        {
            startEndAnimation();
        };
    }
    else if(m_lock->getType() == 6)
    {
        if(NodeToClip(m_lock->getChildByTag(3))->getPathProgress() > 0.96 && NodeToClip(m_lock->getChildByTag(4))->getPathProgress() > 0.96)
        {
            NodeToClip(m_lock->getChildByTag(3))->setPathFilter(fLimit);
            NodeToClip(m_lock->getChildByTag(4))->setPathFilter(fLimit);
            startEndAnimation();
        };
    }else if(m_lock->getType() == 8)
    {
        if(NodeToClip(m_lock->getChildByTag(3))->getPathProgress() > 0.94 && state_lock == 2)
        {
            startEndAnimation();
        };
    }else if(m_lock->getType() == 9) //in-yan
    {
        if(mouse_touch[touch->getID()].is_active && mouse_touch[touch->getID()].element)
        {
        if(mouse_touch[touch->getID()].element->getTag() == 12 &&  mouse_touch[touch->getID()].element->getRotation() < 160)
        {
            runTouchEnd(touch->getID());
        }
        else if(mouse_touch[touch->getID()].element->getTag() == 11  && mouse_touch[touch->getID()].element->getRotation() < -20)
        {
            runTouchEnd(touch->getID());
        };
        }
        if(m_lock->getChildByTag(12)->getRotation() < 160 && m_lock->getChildByTag(11)->getRotation() < -20)
        {
            NodeToClip(m_lock->getChildByTag(3))->setEnabled(true);
            if(NodeToClip(m_lock->getChildByTag(3))->getPathProgress() > 0.93)
            {
                startEndAnimation();
            };
        }

    }else if(m_lock->getType() == 10) //apple
    {
        if(NodeToClip(m_lock->getChildByTag(3))->getPathProgress() > 0.8 && NodeToClip(m_lock->getChildByTag(4))->getRotation() > 40)
        {
            startEndAnimation();
        };
    }else if(m_lock->getType() == 11)
    {
        if(NodeToClip(m_lock->getChildByTag(3))->getRotation() > 70 && NodeToClip(m_lock->getChildByTag(4))->getRotation() < -70)
        {
            startEndAnimation();
        };
    }
};

void LayerLockSprite::startEndAnimation()
{
   // m_bracloth->center->removeAllChildrenWithCleanup(true);
    if(m_lock->getChildByTag(99))
    {
        m_lock->removeChild(m_lock->getChildByTag(99), true);
    };
    if(m_lock->getChildByTag(98))
    {
        m_lock->removeChild(m_lock->getChildByTag(98), true);
    };
    
    float time_down = 0.5;
    float time_to_down = 0.25;
    float delay_start_lock  = 0.25;
    float delay_start_lock_back  = 0.5;
    
    start_end_animation = true;
    m_lock->runComplite();
    CCNode* right = m_lock->getChildByTag(2);
    CCNode* left = m_lock->getChildByTag(1);
   // CCDirector::sharedDirector()->getScheduler()->setTimeScale(0.1);
    
    float rot_position_koef = (is_ipad)?1.2:2.0;
    float koef_down_move = (is_ipad)?12.5:28.0;
    //left lock animation
    CCNode * m_sprite = CCNode::create();
    m_sprite->setPosition(ccp(-win_size.width*rot_position_koef,left->getPositionY()));
    m_sprite->setTag(99);
    m_lock->addChild(m_sprite,99);
    length_left = ccpLength(ccpSub(m_sprite->getPosition(), left->getPosition()));
    //CCLog("left pos %f %f",length_left);
    CCActionInterval* action_ease_left = CCEaseSineInOut::create((CCActionInterval*)(CCRotateBy::create(0.45, 280)->copy()->autorelease()));
    CCAction *actions = CCSequence::create(CCDelayTime::create(time_to_down),CCMoveBy::create(time_down, ccp(0.0,-win_size.height*koef_down_move)),NULL);
    CCAction *spaw = CCSpawn::create(action_ease_left,actions,NULL);
    CCAction *action;
    if(m_lock->getType() == 5)
    {
        action = CCSequence::create(CCDelayTime::create(0.0),CCMoveBy::create(delay_start_lock, ccp(-win_size.width*(is_ipad?0.5:1.0),0)),spaw, NULL);
    }
    else
        action = CCSequence::create(CCDelayTime::create(delay_start_lock),spaw, NULL);
    
    m_sprite->runAction(action);
    
    //right lock animation
    CCNode * m_sprite_r = CCNode::create();
    m_sprite_r->setPosition(ccp(win_size.width*rot_position_koef,right->getPositionY()));
    m_sprite_r->setTag(98);
    m_lock->addChild(m_sprite_r,99);
    length_right = ccpLength(ccpSub(m_sprite_r->getPosition(), right->getPosition()));
    CCActionInterval* action_ease_right = CCEaseSineInOut::create((CCActionInterval*)(CCRotateBy::create(0.45, 280)->copy()->autorelease()));
    
    CCAction *actions2 = CCSequence::create(CCDelayTime::create(time_to_down),CCMoveBy::create(time_down, ccp(0.0,-win_size.height*koef_down_move)),NULL);
    CCAction *spaw2 = CCSpawn::create(action_ease_right,actions2,NULL);
 //   CCAction *action2 = CCSequence::create(CCDelayTime::create(delay_start_lock),spaw2 , NULL);
    
    CCAction *action2;
    if(m_lock->getType() == 5)
    {
        action2 = CCSequence::create(CCDelayTime::create(0.0),CCMoveBy::create(delay_start_lock, ccp(win_size.width*(is_ipad?0.5:1.0),0)),spaw2, NULL);
    }
    else
        action2 = CCSequence::create(CCDelayTime::create(delay_start_lock),spaw2, NULL);
    
    m_sprite_r->runAction(action2);

    m_bracloth->center->runAction(CCSequence::create(CCDelayTime::create(delay_start_lock_back),CCMoveTo::create(time_down,ccp(m_bracloth->center->getPositionX(),-win_size.height*koef_down_move)),NULL));
    
    runAction(CCCallFuncND::create(getParent()->getParent(), callfuncND_selector(BGameScene::lockComplite),getParent()));
    BraHelper::Instance()->curent_girl->back_bra->runAction(CCSequence::create(CCDelayTime::create(delay_start_lock_back),CCMoveBy::create(time_down,ccp(0,-win_size.height*4.0)),NULL));

};
void LayerLockSprite::startTouch()
{
    this->setTouchEnabled(true);
    start_end_animation = false;
    scheduleUpdate();
 //   m_lock->setPosition(ccp(getContentSize().width*0.5,getContentSize().height*0.5));
};

bool LayerLockSprite::start()
{
    state_lock = 0;
   // runAction(CCSequence::create(CCDelayTime::create(0.1), CCCallFunc::create(this,callfunc_selector(LayerLockSprite::startTouch)),NULL));
    startTouch();
    return true;
};
void LayerLockSprite::update(float _dt)
{
    if(start_end_animation)
    {
        if(getZOrder() != 0)
        {
            if(is_ipad)
            {
                if(m_bracloth->center->getPositionY() < -win_size.height*0.5)
                {
                    this->getParent()->reorderChild(this, 0);
                };
            }
            else
            {
                if(m_bracloth->center->getPositionY() < -win_size.height*1.5)
                {
                    this->getParent()->reorderChild(this, 0);
                };
            }
        }
        float angel = m_lock->getChildByTag(99)->getRotation();
        CCPoint newpos = ccpForAngle(-1*CC_DEGREES_TO_RADIANS(angel - 180));
        newpos = ccpSub(m_lock->getChildByTag(99)->getPosition(),ccpMult(newpos, length_left));
        //CCLOG("rotation %f %f %f",angel,newpos.x,newpos.y);
        m_lock->getChildByTag(1)->setPosition(newpos);
        
        float angel2 = m_lock->getChildByTag(98)->getRotation();
        CCPoint newpos2 = ccpForAngle(1*CC_DEGREES_TO_RADIANS(angel2 - 180));
        newpos = ccpSub(m_lock->getChildByTag(98)->getPosition(),ccpMult(newpos2, -length_right));
        //CCLOG("rotation %f %f %f",angel,newpos.x,newpos.y);
        m_lock->getChildByTag(2)->setPosition(newpos);
        
    }
    m_bracloth->update();
};

bool LayerLockSprite::end()
{
    unscheduleUpdate();
    return true;
};

void LayerLockSprite::reset()
{
    setTouchEnabled(false);
    m_lock->setVisible(false);
    LockPoolSingleton::Instance()->add(m_lock);
    m_lock = NULL;

    start_end_animation = true;
    istouch = false;
    for (int i = 0; i < 5; ++i) {
        mouse_touch[i].element = NULL;
        mouse_touch[i].is_active = false;
    };
    
    count_touch = 0;
    
   loadGirlPose();
    
};

void LayerLockSprite::initBra(const char* _name, const CCPoint &_tl1,const CCPoint &_bl1,const CCPoint &_tr1,const CCPoint &_br1)
{
    float Scale = 0.5;
    this->setScale(Scale);
    
    Scale = BraHelper::Instance()->camera_scale;
    this->setScale(BraHelper::Instance()->camera_scale);
    
    //m_bracloth = BraPoolSingleton::Instance()->get();
    m_bracloth->center->setAnchorPoint(ccp(0.5,0.5));
    
    m_bracloth->center->initWithPoint(_tl1,_bl1,_tr1,_br1);
    
    float width_ = win_size.width;
    if(win_size.width == 1024) //ipad
        m_bracloth->center->setPosition(ccp(width_*1.0,win_size.height*0.625));
    else
    {
       m_bracloth->center->setPosition(ccp(width_*0.75,win_size.height*0.597)); 
    }
//    if(win_size.width == 480)
//          m_bracloth->center->setPosition(ccp(width_*0.75,win_size.height*0.597));
//    if(win_size.width == 568)
    
    
    while (m_lock == NULL) {
        m_lock = LockPoolSingleton::Instance()->get();
        std::list<int>::iterator it = BraHelper::Instance()->previor_lock_id.begin();
        bool p = false;
        while (it != BraHelper::Instance()->previor_lock_id.end()) {
            if(*it == m_lock->getType())
            {
                LockPoolSingleton::Instance()->add(m_lock);
                m_lock = NULL;
                p = true;
                break;
            };
            it++;
        };
    };
    
    CCLOG("LayerLockSprite::initBra LockPoolSingleton::Instance() %d",LockPoolSingleton::Instance()->size());
    CCAssert(m_lock != NULL, "Error");
    m_lock->setVisible(true);
    m_lock->setPosition(ccp(getContentSize().width*lock_start_position.x,getContentSize().height*lock_start_position.y));
    if (m_lock->isLoad())
    {
        m_lock->retain();
        m_lock->removeFromParentAndCleanup(false);
        addChild(m_lock,1);
        CCLOG("m_lock %d",m_lock);
        m_lock->reset();
        m_lock->autorelease();
    }
    else
    {
        addChild(m_lock,1);
    };
    
    if(win_size.width != 1024)
    {
        CCArray *m_array = m_lock->getChildren();
        CCObject * obj;
        CCARRAY_FOREACH(m_array, obj)
        {
            static_cast<CCNode* >(obj)->setScale(2.0);
        };
    };
    lock_num = m_lock->getType();
    lock_type = m_lock->getType();
    
    this->getParent()->reorderChild(this, 10);
    
    if(!m_bracloth->center->getParent())
        addChild(m_bracloth->center,0);
    
    m_bracloth->center->setVisible(false);
    
    CCNode * left_n = m_lock->getChildByTag(1);
    CCNode * right_n = m_lock->getChildByTag(2);
    CCPoint p_left = left_n->getPosition();
    CCPoint p_right = right_n->getPosition();

    p_left = ccpAdd(p_left, m_lock->getPosition());
    p_right = ccpAdd(p_right, m_lock->getPosition());

    //CCLOG("points :: _tl1(%f %f),_bl1(%f %f),_tr1(%f %f),_br1(%f %f)",_tl1.x,_tl1.y,_bl1.x,_bl1.y,_tr1.x,_tr1.y,_br1.x,_br1.y);

    if(win_size.width == 1024) //ipad
    {
        m_bracloth->left->initWithTwoSprite(m_bracloth->center,(CCSprite*)(m_lock->getChildByTag(1)),ccp(_tl1.x,_tl1.y),ccp(_bl1.x,_bl1.y),ccp(0,getContentSize().height*0.125),ccp(0,-getContentSize().height*0.125),6);
        m_bracloth->right->initWithTwoSprite(m_bracloth->center,(CCSprite*)(m_lock->getChildByTag(2)), ccp(_tr1.x,_tr1.y), ccp(_br1.x,_br1.y), ccp(0,getContentSize().height*0.125),ccp(0, -getContentSize().height*0.125),6) ;
    }
    else
    {
        m_bracloth->left->initWithTwoSprite(m_bracloth->center,(CCSprite*)(m_lock->getChildByTag(1)),ccp(_tl1.x,_tl1.y),ccp(_bl1.x,_bl1.y),ccp(0,getContentSize().height*0.25),ccp(0,-getContentSize().height*0.25),6);
        m_bracloth->right->initWithTwoSprite(m_bracloth->center,(CCSprite*)(m_lock->getChildByTag(2)), ccp(_tr1.x,_tr1.y), ccp(_br1.x,_br1.y), ccp(0,getContentSize().height*0.25),ccp(0, -getContentSize().height*0.25),6) ;
    }
    if(!m_bracloth->left->getParent())
        addChild(m_bracloth->left,0);
    if(!m_bracloth->right->getParent())
        addChild(m_bracloth->right,0);
    m_bracloth->update();
};
void LayerLockSprite::loadGirlPose()
{
    char str[NAME_MAX];
    sprintf(str, "girl%d",BraHelper::Instance()->num_girl);
    CCDictionary *gdict  = static_cast<CCDictionary *>(dictDict(m_dict,"girl")->objectForKey(str));
    CCDictionary *bdict = dictDict(gdict,"bra");
    
    CCPoint p_tl = CCPointFromString(dictStr(bdict, "left_top"));
    CCPoint p_bl = CCPointFromString(dictStr(bdict, "left_bottom"));
    CCPoint p_tr = CCPointFromString(dictStr(bdict, "right_top"));
    CCPoint p_br = CCPointFromString(dictStr(bdict, "right_bottom"));
    CCPoint p_center = ccp(0.0,0.0);
    
    
    
    float koef = 1.0;
    if(win_size.width == 568)
        koef = 0.845f;
    lock_start_position = ccp(0.5,0.5);

    if(gdict->objectForKey("lock_position"))
        lock_start_position = CCPointFromString(dictStr(gdict, "lock_position"));
    
    CCPoint tl1,bl1,tr1,br1;
    tl1 = ccp(win_size.width*(p_center.x + p_tl.x)*koef,win_size.height*(p_center.y + p_tl.y));
    bl1 = ccp(win_size.width*(p_center.x + p_bl.x)*koef,win_size.height*(p_center.y + p_bl.y));
    
    tr1 = ccp(win_size.width*(p_center.x + p_tr.x)*koef,win_size.height*(p_center.y + p_tr.y));
    br1 = ccp(win_size.width*(p_center.x + p_br.x)*koef,win_size.height*(p_center.y + p_br.y));
    
    sprintf(str, "lock1",BraHelper::Instance()->getNewType());
    BraHelper::Instance()->camera_scale = dictFloat(dictDict(dictDict(m_dict,"lock"),str),"camera");
    
    initBra(dictStr(dictDict(dictDict(m_dict,"lock"),str),"plist"),tl1,bl1,tr1,br1);
};

bool LayerLockSprite::createWithPlist(const char* _filename)
{
    CCLOG("LayerLockSprite::createWithPlist %s",_filename);
     
    m_dict = CCDictionary::createWithContentsOfFile(_filename);
    m_dict->retain();
    
    loadGirlPose();
    return true;
};