//
//  LayerBra.h
//  Bra
//
//  Created by PR1.Stigol on 26.02.13.
//
//

#ifndef __Bra__LayerBra__
#define __Bra__LayerBra__
#include "cocos2d.h"
#include <iostream>
#include "Component\BComponent.h"

using namespace cocos2d;

class BaseLayerLock: public cocos2d::CCLayer
{
protected:
    cocos2d::CCSize win_size;
    
    CCDictionary *m_dict;
    int lock_num;
    CCPoint lock_start_position;
public:
    BraCloth *m_bracloth;
    virtual bool start() = 0;
    virtual bool end() = 0;
    virtual void reset() = 0;
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event) = 0;
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event) = 0;
    virtual void ccTouchesMoved(cocos2d::CCSet* touches, cocos2d::CCEvent* event) = 0;
    virtual void draw() = 0;
    virtual bool createWithPlist(const char* _filename) = 0;
    BaseLayerLock()
    {
        win_size = CCDirector::sharedDirector()->getWinSize();
    };
    virtual ~BaseLayerLock()
    {
    };
    const CCPoint & getLockStartPosition()
    {
        return lock_start_position;
    };
    
    int getLockNumber() const
    {
        return lock_num;
    };
};



#endif /* defined(__Bra__LayerBra__) */
