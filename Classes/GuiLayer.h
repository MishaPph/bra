//
//  GuiLayer.h
//  Bra
//
//  Created by PR1.Stigol on 13.03.13.
//
//

#ifndef __Bra__GuiLayer__
#define __Bra__GuiLayer__

#include <iostream>
#include "Component\BComponent.h"
#include "cocos2d.h"

using namespace cocos2d;

class GUILayer: public BComponent
{
private:
    CCObject*  m_listener;
    std::map<std::string, SEL_CallFuncND> func_map;
    BElement* curent_node;
    bool touch_enabled;
public:
    GUILayer();
    virtual ~GUILayer();
    void setTarget(CCObject *rec);
    void setSelector(SEL_MenuHandler _selector);
    void addFuncToMap(const std::string& _name,SEL_CallFuncND _func_sel);
    bool setTouchBegan(const CCPoint &_pos);
    void setTouchMove(const CCPoint &_pos);
    void setTouchEnd(const CCPoint &_pos);
    static GUILayer * create(void);
    void setTouchEnabled(bool _touch);
    bool isTouchEnabled();
};
#endif /* defined(__Bra__GuiLayer__) */
