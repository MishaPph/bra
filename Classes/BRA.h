//
//  BRA.h
//  Bra
//
//  Created by PR1.Stigol on 25.01.13.
//
//

#ifndef Bra_BRA_h

#define NAME_MAX 255

#define Bra_BRA_h

#include "cocos2d.h"
#include <algorithm>
#include <random>

#define DEBUG_MODE 0
#define SHOW_BODY_OBJECT 0
#define DISABLE_UPDATE 0
#define BASE_LAYER_SCALE  0.15
#define PTM_RATIO 32.0
#define PI 3.14159
#define PI180 57.2958
//#define DISABLE_BACKGROUND_SOUND
//static float PTM_RATIO = 32.0;

#define dictStr(__dict__,__str__) ((CCString *)__dict__->objectForKey(__str__))->getCString()
#define dictInt(__dict__,__str__)((CCString *) __dict__->objectForKey(__str__))->intValue()
#define dictFloat(__dict__,__str__)((CCString *) __dict__->objectForKey(__str__))->floatValue()
#define dictDict(__dict__,__str__)static_cast<CCDictionary *>(__dict__->objectForKey(__str__))
#define dictArray(__dict__,__str__)static_cast<CCArray *>(__dict__->objectForKey(__str__))

#define NodeToClip(_sprite_)static_cast<BElementClip *>(_sprite_)

#define FOR_FIXTURE(_d) for(b2Fixture* fixture = _d->GetFixtureList();fixture;fixture = fixture->GetNext())
#define FOR_JOINT(_d) for(b2JointEdge* joint = _d->GetJointList();joint;joint = joint->next)

template <class T>
class Singleton
{
public:
    static T* Instance() {
        if(!m_pInstance) m_pInstance = new T;
        assert(m_pInstance != NULL);
        return m_pInstance;
    }
protected:
    Singleton();
    ~Singleton();
private:
    Singleton(Singleton const&);
    Singleton& operator=(Singleton const&);
    static T* m_pInstance;
};

template <class T> T* Singleton<T>::m_pInstance = NULL;

template <typename _Tp>
class BPool {
    std::list< _Tp > pool;
    typename std::list< _Tp >::iterator it;
public:
    unsigned int size() const
    {
        return pool.size();
    };
    void add(const _Tp &tp)
    {
        pool.push_back(tp);
    };
    _Tp get()
    {
        if(pool.empty())
            return NULL;
        CCAssert(!pool.empty(), "end");
        _Tp tmp = pool.front();
        pool.pop_front();
        return tmp;
    };
    void resort()
    {
        //std::vector<_Tp> buf(pool.begin(), pool.end());
//		auto rng = std::default_random_engine{};

      //  std::shuffle(buf.begin(), buf.end(), pool);
        //std::copy(buf.begin(), buf.end(), pool.begin());
		//std::shuffle(std::begin(pool), std::end(pool), rng);
	};
    void clear()
    {
        pool.clear();
    };
};
class BComponent;
struct BraCloth;
struct BGirl;
class BaseLayerLock;
typedef Singleton<BPool<BComponent* > > LockPoolSingleton;
typedef Singleton<BPool<BraCloth* > > BraPoolSingleton;
typedef Singleton<BPool<BGirl* > > GirlPoolSingleton;


namespace BRA {
    const float TIME_ANIMATION_ZOOM = 0.5;
    static kmVec2 center;
    enum touch_state
    {
        RETURN = 0,
        MONO = 1,
        MOVE = 2,
        TWO_MONO = 3,
        BEGIN = 4,
        MOVE_OBJECT = 5,
        START_PHYSICS = 6,
        END_PHYSICS = 7
    };
    enum Etype
    {
        elNONE          = 0x00000000,
        elBUTTON        = 0x00000001,
        elCLIPPER       = 0x00000010,
        elPICTURE       = 0x00000100,
        elCHECKBOX      = 0x00001000,
        elRADIOBUTTON   = 0x00010000
    };
    enum Action
    {
        actionPLAY = 1,
        actionEXIT = 2,
        actionNEXT = 3,
        actionREPLAY = 4,
        actionVIDEO = 5,
        actionMENU = 6
    };
    static std::string getFileWithoutSuffic(const std::string & _file)
    {
        int nPosRight  = _file.find('.');
        return _file.substr(0, nPosRight);
    };
    static std::string loadImage(const char* _name,bool _retina = false)
    {
        char tmp[NAME_MAX];
        std::string tmp_str = _name;
        int nPosRight  = tmp_str.find('.');
        
        std::string _sufix = tmp_str.substr(nPosRight + 1,tmp_str.length());
        tmp_str = tmp_str.substr(0, nPosRight);
        
        //   CCLOG("String %s.%s",tmp_str.c_str(),_sufix.c_str());
        
        if(_retina)
        {
            if(cocos2d::CCDirector::sharedDirector()->getContentScaleFactor() != 2.0)
            {
                if(cocos2d::CCDirector::sharedDirector()->getWinSize().width != 1024)
                    sprintf(tmp, "%s.%s", tmp_str.c_str(),_sufix.c_str());
                else
                    sprintf(tmp, "%s_p.%s", tmp_str.c_str(),_sufix.c_str());
            }
            else
            {
                if(cocos2d::CCDirector::sharedDirector()->getWinSize().width != 1024)
                    sprintf(tmp, "%s@2x.%s", tmp_str.c_str(),_sufix.c_str());
                else
                    sprintf(tmp, "%s_p@2x.%s", tmp_str.c_str(),_sufix.c_str());
            };
        }
        else
        {
            if(cocos2d::CCDirector::sharedDirector()->getWinSize().width != 1024)
                sprintf(tmp, "%s_iphone.%s", tmp_str.c_str(),_sufix.c_str());
            else
                sprintf(tmp, "%s_p.%s", tmp_str.c_str(),_sufix.c_str());
        }
        
         CCLOG("loadImage::new file name %s",tmp);
        return tmp;
    };

    static bool strToTwoStr(const std::string &_str, std::string &_left,std::string &_right,const char _sep)
    {
        std::string tmp_str = _str;
        int nPosRight  = tmp_str.find(_sep);
        _left = tmp_str.substr(0, nPosRight);
        _right = tmp_str.substr(nPosRight + 1, tmp_str.length() - nPosRight);
        return !(nPosRight == -1);
    };
//    static kmVec2 b2Vec2FromString(const char* pStr)
//    {
//            std::string content = pStr;
//            int nPosLeft  = content.find('{');
//            int nPosRight = content.find('}');
//            std::string pointStr = content.substr(nPosLeft + 1, nPosRight - nPosLeft - 1);
//         //   int PosLeft  = pointStr.find(',');
//      //  return kmVec2(atof(pointStr.substr(0, PosLeft).c_str())/PTM_RATIO,atof(pointStr.substr(PosLeft+1,pointStr.length()).c_str())/PTM_RATIO);
//    };
    
//    static void forceToBody(b2Fixture* _fixture,b2Body* _body,float _force,bool _lenght = true);
//    {
//        b2CircleShape* circle = (b2CircleShape*)_fixture->GetShape();
//        b2Vec2 center = _fixture->GetBody()->GetWorldPoint(circle->m_p);
//        b2Vec2 position = _body->GetPosition();
//        b2Vec2 d = center - position;
//        
//        float l = (_lenght)?d.Length():1.0;
//        d.Normalize();
//        b2Vec2 F = _force * l * d ;
//        _body->ApplyForce(F, position);
//    };
};

#endif
