//
//  BSpriteBody.h
//  Bra
//
//  Created by PR1.Stigol on 25.01.13.
//
//

#ifndef __Bra__BSpriteBody__
#define __Bra__BSpriteBody__
#include "cocos2d.h"

#include <iostream>
#include "BRA.h"

class BraJoinSprite: public cocos2d::CCSprite
{
public:
    BraJoinSprite();
    static BraJoinSprite* create(void);
    static void clearAll()
    {
        list_bra.clear();
    };
    static void updateAll();

    void initWithTwoSprite(CCSprite* _left_body,CCSprite* _right_body,const cocos2d::CCPoint& _o_lt,const cocos2d::CCPoint& _o_lb,const cocos2d::CCPoint& _o_rt,const cocos2d::CCPoint& _o_rb,int _type = 6);
    void changeWithTwoSprite(CCSprite* _left_body,CCSprite* _right_body,const cocos2d::CCPoint& _o_lt,const cocos2d::CCPoint& _o_lb,const cocos2d::CCPoint& _o_rt,const cocos2d::CCPoint& _o_rb,bool _replace = false);


    const kmVec2 & getVertext(const int &_index) const;
    void destroyJoin();
    void updateBody();
    void setScale(const float &_scale);
    void setScaleBody(const int &_index,const float &_scale)
    {
        m_scale[_index] = _scale;
    };
    void setOffsetBody(const int &_index,const cocos2d::CCPoint &_off)
    {
        m_vect2[_index] = _off;
    };
    void initWithPoint(const cocos2d::CCPoint &_tl,const cocos2d::CCPoint &_bl,const cocos2d::CCPoint &_tr,const cocos2d::CCPoint &_br);
    cocos2d::ccV3F_C4B_T2F_Quad getQuad() const
    {
        return m_sQuad;
    };
    void initWithTwoSprite(CCSprite* _left_body,CCSprite* _right_body,int _type);
private:
    float distance_x1,distance_x2;
    cocos2d::CCPoint offset_lt,offset_lb,offset_rt,offset_rb;
    static const int COUN_POINT = 4;
    static  std::list<BraJoinSprite *> list_bra;
    void createJoint();
    CCSprite* m_sprite[COUN_POINT];
    float m_scale[COUN_POINT];
    cocos2d::CCPoint m_vect2[COUN_POINT];
    int type;
};

struct BraCloth
{
    BraJoinSprite* right,*left,*center;
    BraCloth(const char* _texturename)
    {
        center = BraJoinSprite::create();
        center->retain();
        left = BraJoinSprite::create();
        left->retain();
        right = BraJoinSprite::create();
        right->retain();
        center->initWithTexture(cocos2d::CCTextureCache::sharedTextureCache()->addImage(_texturename));
        
        left->initWithTexture(cocos2d::CCTextureCache::sharedTextureCache()->addImage(_texturename));

        right->initWithTexture(cocos2d::CCTextureCache::sharedTextureCache()->addImage(_texturename));
    };
    void removeFromParent()
    {
        center->removeFromParentAndCleanup(false);
        left->removeFromParentAndCleanup(false);
        right->removeFromParentAndCleanup(false);
    };
    void setTextureName(const char* _texturename)
    {
        center->setTexture(cocos2d::CCTextureCache::sharedTextureCache()->addImage(_texturename));
        left->setTexture(cocos2d::CCTextureCache::sharedTextureCache()->addImage(_texturename));
        right->setTexture(cocos2d::CCTextureCache::sharedTextureCache()->addImage(_texturename));
    };
    void setTextureID(const char* _Framename)
    {
      //  CCLOG("_Framename:: %s",_Framename);
        cocos2d::CCSpriteFrame* frame = cocos2d::CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(_Framename);
        if(!frame)
        {
            CCLOG("Error::setTextureID %s",_Framename);
            return;
        }
        center->setTexture(frame->getTexture());
        center->setTextureRect(frame->getRect(),frame->isRotated(),frame->getOriginalSize());
        left->setTexture(frame->getTexture());
        left->setTextureRect(frame->getRect(),frame->isRotated(),frame->getOriginalSize());
        right->setTexture(frame->getTexture());
        right->setTextureRect(frame->getRect(),frame->isRotated(),frame->getOriginalSize());
        


    };
    void update()
    {
        right->updateBody();
        left->updateBody();
        center->updateBody();
    };
    void setScale(const float &_scale)
    {
        right->setScale(_scale);
        left->setScale(_scale);
        center->setScale(_scale);
    };
    ~BraCloth()
    {
        CCLOG("BraCloth::~BraCloth");
        CC_SAFE_RELEASE_NULL(right);
        CC_SAFE_RELEASE_NULL(left);
        CC_SAFE_RELEASE_NULL(center);
    };
};
#endif /* defined(__Bra__BSpriteBody__) */
