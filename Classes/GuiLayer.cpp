//
//  GuiLayer.cpp
//  Bra
//
//  Created by PR1.Stigol on 13.03.13.
//
//

#include "GuiLayer.h"

GUILayer::GUILayer()
{
    curent_node = NULL;
};

GUILayer::~GUILayer()
{
    CCLOG("~GUILayer:: destroy ");
};

void GUILayer::addFuncToMap(const std::string& _name,SEL_CallFuncND _func_sel)
{
    CCLOG("addFuncToMap:: name %s",_name.c_str());
    func_map[_name] = _func_sel;
};

void GUILayer::setTouchEnabled(bool _touch)
{
    touch_enabled = _touch;
}

bool GUILayer::isTouchEnabled()
{
    return touch_enabled;
};

void GUILayer::setTarget(CCObject *_rec)
{
    m_listener = _rec;
};

bool GUILayer::setTouchBegan(const CCPoint &_pos)
{
    if(!touch_enabled|| !isVisible())
        return false;
    int type = BRA::elBUTTON | BRA::elCHECKBOX | BRA::elRADIOBUTTON;
    curent_node = getTouchElement(_pos,type);
    if(curent_node)
    {
        CCLOG("CKLoaderGUI::setTouchBegan type %dtag = %d",curent_node->getType(),curent_node->getTag());
        //curent_node->beginTouch();
        if(curent_node->getType() == BRA::elBUTTON)
            curent_node->setState(1);
        
        return true;
    };
    return false;
};

void GUILayer::setTouchMove(const CCPoint &_pos)
{
    if(!touch_enabled || !isVisible())
        return;
    if(!curent_node)
        return;
};

GUILayer * GUILayer::create(void)
{
    GUILayer * pRet = new GUILayer();
	pRet->autorelease();
	return pRet;
};

void GUILayer::setTouchEnd(const CCPoint &_pos)
{
    if(!touch_enabled|| !isVisible())
        return;
    
    if(curent_node)
    {
        if(curent_node->getType() == BRA::elBUTTON)
            curent_node->setState(0);
        else if(curent_node->getType() == BRA::elCHECKBOX)
        {
            static_cast<BElementCheckBox *>(curent_node)->setChecked(!static_cast<BElementCheckBox *>(curent_node)->isChecked());
        }else if(curent_node->getType() == BRA::elRADIOBUTTON)
        {
           // static_cast<BElementRa *>(curent_node)->setChecked(!static_cast<BElementCheckBox *>(curent_node)->isChecked());
        }
        int type = BRA::elBUTTON | BRA::elCHECKBOX | BRA::elRADIOBUTTON;
        
        if(curent_node == getTouchElement(_pos,type))
        {
            if(func_map[curent_node->getFuncName()] != NULL)
            {
                (m_listener->*func_map[curent_node->getFuncName()])(curent_node,(void *)(curent_node->getValue()));
            }
            else
            {
                CCLOG("setTouchEnd:: not function name %s",curent_node->getFuncName().c_str());
            }
        }
    };
};