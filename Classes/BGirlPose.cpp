//
//  BGirlPose.cpp
//  Bra
//
//  Created by PR1.Stigol on 21.03.13.
//
//

#include "BGirlPose.h"
#include "BraHelper.h"

#pragma mark BGIRL POSE
BGirlPose::BGirlPose()
{
    BraHelper::Instance()->count_girl_pose++;
    CCLOG("BGirlPose::BGirlPose() count %d",BraHelper::Instance()->count_girl_pose);
};

BGirlPose::~BGirlPose()
{
    BraHelper::Instance()->count_girl_pose--;
    CCLOG("destroy ~BGirlPose count %d",BraHelper::Instance()->count_girl_pose);
    m_pose->removeFromParentAndCleanup(true);
};

void BGirlPose::randHair()
{
    int count_hair = 12;
    if(type_pose == 1)//realistic
    {
        count_hair = 12;
    }
    else
    {
        count_hair = 12;
    };
    
    bool p = true;
    while (p) {
        //    p = false;
        hair_num = rand()%count_hair;
        if(!BraHelper::Instance()->isHairAcitve(hair_num))
        {
            BraHelper::Instance()->addHairToList(hair_num);
            hair->setState(hair_num);
            return;
        };
    };
    
};

void BGirlPose::setType(const int& _type)
{
    type_pose = _type;
};

void BGirlPose::randGirl()
{
    if(type_pose == 1)//realistic
    {
        girl_num = rand()%3;
    }
    else
    {
        girl_num = rand()%6;
    }
    girl->setState(girl_num);
};

void BGirlPose::animGirlFrame(const int &_frame)
{
    girl->setState(rand()%6);
    char str[NAME_MAX];
    sprintf(str, "FinalAnimation%d%d.png",_frame,_frame);
    CCSpriteFrame * sframe = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(BRA::loadImage(str).c_str());
    static_cast<CCSprite *>(girl)->setTexture(sframe->getTexture());
    static_cast<CCSprite *>(girl)->setTextureRect(sframe->getRect(),sframe->isRotated(),sframe->getOriginalSize());
};

void BGirlPose::randBikini()
{
    if(type_pose == 1)//realistic
    {
        bikini_num = rand()%3;
    }
    else
    {
        bikini_num = rand()%3;
    }
    
    bikini->setState(bikini_num);
};

void BGirlPose::create(int _pose)
{
    m_pose = BComponent::create();
    num = _pose;
    //m_pose->retain();
    
    char str[NAME_MAX];
    if(type_pose == 1)//realistic
    {
        sprintf(str, "c_RealGirl%d.plist",_pose);
    }
    else
    {
        sprintf(str, "c_Girl%d.plist",_pose);
    };
    
    m_pose->loadWithFile(BRA::loadImage(str).c_str());
    win_size = CCDirector::sharedDirector()->getWinSize();
    m_pose->setPosition(ccp(win_size.width/2,win_size.height/2));
    hair = static_cast<BElement *>(m_pose->getChildByTag(2));
    bikini = static_cast<BElement *>(m_pose->getChildByTag(3));
    girl = static_cast<BElement *>(m_pose->getChildByTag(1));
};
