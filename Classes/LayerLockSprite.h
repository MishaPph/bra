//
//  LayerLockSprite.h
//  Bra
//
//  Created by PR1.Stigol on 01.03.13.
//
//

#ifndef __Bra__LayerLockSprite__
#define __Bra__LayerLockSprite__

#include <iostream>
#include "LayerBra.h"
#include "Component\BElementButton.h"

struct MouseTouch
{
    BElementButton *element;
    bool is_active;
    int count_active_element;
};


class LayerLockSprite : public BaseLayerLock {
public:
    ~LayerLockSprite();
    LayerLockSprite();
    static BaseLayerLock* create();
    virtual void draw();
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual bool createWithPlist(const char* _filename);
    virtual bool start();
    virtual bool end();
    virtual void reset();
    void startEndAnimation();
    void update(float _dt);
    void runSlap();
    void startTouch();
    void loadGirlPose();

    void initBra(const char* _name, const CCPoint &_tl1,const CCPoint &_bl1,const CCPoint &_tr1,const CCPoint &_br1);
    int state_lock;
    static int count_object;
private:
    float length_left,length_right;
    bool complited;
    bool istouch;
    MouseTouch mouse_touch[5];
    int lock_type;
    //BElement *element;
    CCPoint base_point;
    void runTouchEnd(const int& _touchId);
    
    BComponent *m_lock;
    bool is_ipad;
    int count_length_null;
    bool start_end_animation;
    unsigned int slide_off_broken;
    //BraJoinSprite* right_bra,*left_bra,*center_bra;
    
    int count_touch;

};
#endif /* defined(__Bra__LayerLockSprite__) */
