//
//  HelloWorldScene.cpp
//  Bra
//
//  Created by PR1.Stigol on 23.01.13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//
#include "BGameScene.h"
#include "LayerLockSprite.h"
#include "EndGameScene.h"
#include "SimpleAudioEngine.h"
#include "Component\BElementClip.h"

using namespace cocos2d;
#define UNIQUE_GIRL false
const int TIME_FIRS_UNLOCK = 4; 
const int TIME_GAME = 120; //time active game session in the seconds
const float TIME_ANIMATION_BTN = 0.3;
const float HAIR_ANIM_UP = 100;
void BParallax::create()
{
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(BRA::loadImage("a_Parallax.plist").c_str());
    sp[0] = CCSprite::createWithSpriteFrameName("Back1.png");
    sp[1] = CCSprite::createWithSpriteFrameName("Back2.png");
    sp[2] = CCSprite::createWithSpriteFrameName("Back3.png");
    sp[3] = CCSprite::createWithSpriteFrameName("Back4.png");
    sp[0]->setAnchorPoint(ccp(0,0));
    sp[1]->setAnchorPoint(ccp(0,0));
    sp[2]->setAnchorPoint(ccp(0,0));
    sp[3]->setAnchorPoint(ccp(0,0));
    
    sp[0]->setPosition(ccp(0,0));
    sp[1]->setPosition(ccp(sp[0]->getPositionX() + sp[0]->getContentSize().width*0.5 + sp[1]->getContentSize().width*0.5 - 1,0));
    sp[2]->setPosition(ccp(sp[1]->getPositionX() + sp[1]->getContentSize().width*0.5 + sp[2]->getContentSize().width*0.5 - 2,0));
    sp[3]->setPosition(ccp(sp[2]->getPositionX() + sp[2]->getContentSize().width*0.5 + sp[3]->getContentSize().width*0.5 - 3,0));
    node->addChild(sp[0],0);
    node->addChild(sp[1],0);
    node->addChild(sp[2],0);
    node->addChild(sp[3],0);
    ccTexParams par;
    par.magFilter = GL_NEAREST;
    par.minFilter = GL_NEAREST;
    
    sp[0]->getTexture()->setTexParameters(&par);
    last_x = sp[3]->getPositionX();
    touches = GUILayer::create();
    touches->retain();
    touches->loadWithFile(BRA::loadImage("parallaxCocoa.plist").c_str());
  //  node->addChild(touches,1);
//    CCSize win_size = CCDirector::sharedDirector()->getWinSize();
//    for(int i = 0; i < 4;++i)
//    {
//        if((sp[i]->getPositionX() >= -sp[i]->getContentSize().width*0.5)&&(sp[i]->getPositionX() <= win_size.width + sp[i]->getContentSize().width*0.5))
//        {
//            sp[i]->setVisible(true);
//        }
//        else
//        {
//            sp[i]->setVisible(false);
//        }
//    };
};

BParallax::~BParallax()
{
    CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFrameByName(BRA::loadImage("a_ParallaxJpeg.plist").c_str());
    node->removeAllChildrenWithCleanup(true);
};

void BParallax::next()
{
    node->setVisible(true);
    int moveX = -210;
    for(int i = 0; i < 4;++i)
    {
        if((sp[i]->getPositionX()) < -sp[i]->getContentSize().width)
        {
            int k =i;
            k--;
            if(k < 0)
                k = 3;
            sp[i]->setPositionX(sp[k]->getPositionX() + sp[k]->getContentSize().width*0.5 + sp[i]->getContentSize().width*0.5 + moveX - 3);
         //   sp[i]->runAction(CCMoveBy::create(BRA::TIME_ANIMATION_ZOOM, ccp(moveX,0)));
        }
        else
        {
            sp[i]->runAction(CCMoveBy::create(BRA::TIME_ANIMATION_ZOOM, ccp(moveX,0)));
        };
    };
    CCArray *m_array = touches->getChildren();
    CCObject * obj = NULL;
    CCARRAY_FOREACH(m_array, obj)
    {
        CCNode * node = static_cast<CCNode*>(obj);
        if (node->getPositionX() + touches->getPositionX() < 100) {
            node->setPositionX(node->getPositionX()+ 4096);
        };
    };
    
    touches->runAction(CCMoveBy::create(BRA::TIME_ANIMATION_ZOOM, ccp(moveX,0)));
    
   // CCSize win_size = CCDirector::sharedDirector()->getWinSize();

};

#pragma mark BGIRL
BGirl::~BGirl()
{
    CCLOG("~BGirl::destroy");
    CC_SAFE_DELETE(m_girl);
};

void BGirl::reset()
{
    m_girl->randGirl();
    m_girl->randHair();
    m_girl->randBikini();
    BraHelper::Instance()->num_girl = m_girl->num;
    is_unlock = false;

};

void BGirl::create(const int &_pose)
{
    CCLOG("BGirl::create");
    win_size = CCDirector::sharedDirector()->getWinSize();
#if !DEBUG_MODE
    char str[NAME_MAX];
    sprintf(str, "girl%d.png",BraHelper::Instance()->num_girl);
    CCLog("load::girl_num %d",BraHelper::Instance()->num_girl);
    
    node = CCNode::create();
    node->setVisible(false);
    m_girl = new BGirlPose;
    m_girl->setType(BraHelper::Instance()->getGirlType());
    m_girl->create(_pose);
    m_girl->m_pose->setScale(1.0/BASE_LAYER_SCALE);
    m_girl->randHair();
    m_girl->randBikini();
    m_girl->randGirl();
    node->addChild(m_girl->m_pose,2);
#endif
    node->setScale(BASE_LAYER_SCALE);
    BraHelper::Instance()->setCameraPosition(node->getPosition());
    if(BraHelper::Instance()->getType() == 1)
        back_bra = CCSprite::createWithSpriteFrameName("BraBackReal.png");
    else
        back_bra = CCSprite::createWithSpriteFrameName("BraBack.png");
    
    back_bra->setVisible(false);
    back_bra->setScale(1.0/BASE_LAYER_SCALE);
    m_girl->m_pose->setScale(1.0/BASE_LAYER_SCALE);
    node->addChild(back_bra,1);
    BraHelper::Instance()->num_girl = _pose;
};

#pragma mark - GAME SCENE
BGameScene::~BGameScene()
{
    CCLOG("destroy ~BGameScene");
    curent_layer_bra = NULL;
    removeChild(m_parallax->touches, false);
//    CCLOG("text_hair RetainCount %d LockPoolSingleton::Instance()-> %d",text_hair->retainCount(),LockPoolSingleton::Instance()->size());
    BComponent* tmp = LockPoolSingleton::Instance()->get();
    while (tmp) {
        tmp->unloadAtlasTexture();
        CC_SAFE_RELEASE_NULL(tmp);
        tmp = LockPoolSingleton::Instance()->get();
    };

    for (int i = 0; i < COUNT_UNIQUE_POSE; i++) {
        layer_bra[i]->removeFromParentAndCleanup(false);
        CC_SAFE_DELETE(m_girl[i]);
        CC_SAFE_RELEASE_NULL(layer_bra[i]);
    };
CCNode *m_node = m_parallax->touches;
#if !DEBUG_MODE
    CC_SAFE_DELETE(m_parallax);
#endif
    CC_SAFE_RELEASE_NULL(m_gui);
    CC_SAFE_RELEASE_NULL(m_node);
};

BGameScene::BGameScene()
{
    createAnimation();
    setTouchEnabled( true );
    setAccelerometerEnabled( true );
    start_animation = false;
    win_size = CCDirector::sharedDirector()->getWinSize();
    end_animation = false;
    BraHelper::Instance()->previor_lock_id.clear();
    
    COUNT_UNIQUE_POSE = 3;
    
  //  CCLOG("BraHelper::Instance()->getGirlType(%d)",BraHelper::Instance()->getGirlType());
    // clear_texture.clear();
    if(BraHelper::Instance()->getGirlType() == 1)//realistic
    {
        COUNT_VISIBLE_GIRL = 2;
        //    clear_texture.push_back(CCTextureCache::sharedTextureCache()->addImage(BRA::loadImage("a_HairReal.png").c_str()));
        CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(BRA::loadImage("a_HairReal.plist").c_str());
        
        // clear_texture.push_back(CCTextureCache::sharedTextureCache()->addImage(BRA::loadImage("a_Pose1.png").c_str()));
        //  clear_texture.push_back(CCTextureCache::sharedTextureCache()->addImage(BRA::loadImage("a_Pose2.png").c_str()));
        //  clear_texture.push_back(CCTextureCache::sharedTextureCache()->addImage(BRA::loadImage("a_Pose3.png").c_str()));
    }
    else
    {
        COUNT_VISIBLE_GIRL = 3;
        //  CCTexture2D *texture = CCTextureCache::sharedTextureCache()->addImage(BRA::loadImage("a_Hair.png").c_str());
        //  clear_texture.push_back(texture);
        CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(BRA::loadImage("a_Hair.plist").c_str());
        //  texture->autorelease();
    };
    //text_hair->autorelease();
    
    createPoolObject();
    LockPoolSingleton::Instance()->resort();
    BraHelper::Instance()->num_girl = 1;
	
    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
#ifndef DISABLE_BACKGROUND_SOUND
    if(BraHelper::Instance()->getGirlType() == 1)
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("Background2.mp3",true);
        else
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("ThemeSong.mp3",true);
#endif
    createLayerLock();
    
    createBGirl();
    
    animationGirlPose();
    createParallax();
    createMenu();
    
  //  ObjCCalls::logPageView();//Flurry
  //  ObjCCalls::sendEventFlurry("session_start",false, "girls_type=%d,is_video=%d",BraHelper::Instance()->getGirlType() - 1,BraHelper::Instance()->isRecordVideo());
    
    if(BraHelper::Instance()->isRecordVideo())
    {
        //ObjCCalls::startRecordVideo(TIME_GAME,0);
    };
    generatedBraClothTexture();
    time_game = TIME_GAME;
    count_lock_complite = 0;
    BraHelper::Instance()->printHairList();
    scheduleUpdate();
};

void BGameScene::generatedBraClothTexture()
{
    for (int i = 0 ; i < COUNT_UNIQUE_POSE; ++i) {
        char str[NAME_MAX];
        std::string tmp_str = BRA::getFileWithoutSuffic(m_girl[i]->m_girl->bikini->getFileName());
        
        if(BraHelper::Instance()->getGirlType() == 1)
        {
            int pos = 15;// tmp_str.find(".");
            sprintf(str, "Pose%dBraReal%s.png",m_girl[i]->m_girl->num,tmp_str.substr(pos, tmp_str.length()- pos).c_str());
        }
        else
        {
            int pos = 11;
            sprintf(str, "Pose%dBra%s.png",m_girl[i]->m_girl->num,tmp_str.substr(pos, tmp_str.length()- pos).c_str());
        }
        layer_bra[i]->m_bracloth->setTextureID(str);
        layer_bra[i]->m_bracloth->update();
    }
};

void BGameScene::createLayerLock()
{
    //+++++++++++++++++++++++++layer
    for (int i = 0; i < COUNT_UNIQUE_POSE; ++i) {
        layer_bra[i] = LayerLockSprite::create();
        layer_bra[i]->retain();
        char str[NAME_MAX];
        sprintf(str, "bra%d.png",rand()%3 + 1);
        layer_bra[i]->m_bracloth = new BraCloth(str);
    };
};

void BGameScene::createParallax()
{
#if !DEBUG_MODE
    m_parallax = new BParallax();
    m_parallax->node = CCNode::create();
    addChild(m_parallax->node,0);
    m_parallax->create();
    m_parallax->touches->setPosition(ccp(0,0));
    addChild(m_parallax->touches,0);
    m_parallax->touches->addFuncToMap("createCocoa", callfuncND_selector(BGameScene::createCocoa));
    m_parallax->touches->setTouchEnabled(true);
#endif
};

void BGameScene::createCocoa(CCNode *_sender,void *_d)
{
    CCSprite* m_sprite = CCSprite::create("Cocoa.png");
    m_sprite->setPosition(_sender->getPosition());
    m_sprite->setPositionX(m_sprite->getPositionX() + _sender->getParent()->getPositionX());

    m_sprite->runAction(CCSpawn::create(CCMoveTo::create(1.5, ccp(m_sprite->getPositionX(),-m_sprite->getContentSize().height)),CCRotateBy::create(1.5, rand()%720-360),NULL));
    _sender->getParent()->getParent()->addChild(m_sprite,0);
};

void BGameScene::createBGirl()
{
    //resort not pool
    for (int i = 0; i < 10;++i)
    {
        int k = rand()%3,l = rand()%3;
        if(k != l)
        {
            BGirl *tmp = m_girl[k];
            m_girl[k] = m_girl[l];
            m_girl[l] = tmp;
        }
    };
    int i = 0;
    while (i < COUNT_UNIQUE_POSE) {
        if(BraHelper::Instance()->getGirlType() == 1)
        {
            m_girl[i]->node->setPosition(ccp(win_size.width*(0.2) + win_size.width*0.4*i,win_size.height*0.4));
        }
        else
        {
            m_girl[i]->node->setPosition(ccp(win_size.width*(0.1) + win_size.width*0.3*i,win_size.height*0.4));
        }
        
        m_girl[i]->node->setVisible(i < COUNT_VISIBLE_GIRL);
        m_girl[i]->node->addChild(layer_bra[i],8);
        BraHelper::Instance()->num_girl = m_girl[i]->m_girl->num;
        if(BraHelper::Instance()->getGirlType() == 1)
        {
            layer_bra[i]->createWithPlist(BRA::loadImage("girlreal_locks.plist").c_str());
        }
        else
        {
            layer_bra[i]->createWithPlist(BRA::loadImage("girl_locks.plist").c_str());
        };
        //++++++++++++++++++++++++
        ++i;
    };
};

void BGameScene::createPoolObject()
{
   // char str[NAME_MAX];
    BraPoolSingleton::Instance()->clear();
    GirlPoolSingleton::Instance()->clear();
    LockPoolSingleton::Instance()->clear();
    //fill pool bra cloth
//    for(int i = 0;  i < 3; i++)
//    {
//        sprintf(str, "bra%d.png",rand()%3 + 1);
//        BraCloth* tmp = new BraCloth(str);
//        BraPoolSingleton::Instance()->add(tmp);
//    };
    //fill pool Component lock
    const int COUNT_UNIQ_LOCK = 10;
    int t = rand()%COUNT_UNIQ_LOCK + 1;
    int mas[POOL_SIZE];
    for (int i = 0; i < POOL_SIZE; i++) {
        if(t > COUNT_UNIQ_LOCK)
            t = 0;
        if(t == 6)
            t = 7;
        mas[i] = t;
        t++;
    };
    for (int i = 0; i < POOL_SIZE; i++) {
        int a = rand()%POOL_SIZE,b = rand()%POOL_SIZE;
        t = mas[a];
        mas[a] = mas[b];
        mas[b] = t;
    };
    //create pool lock
    for (int i = 0; i < POOL_SIZE;++i)
    {
        BComponent * tmp = BComponent::create();
        tmp->retain();
        char str[NAME_MAX];
        sprintf(str, "NewLock%d.plist",mas[i]+1);
        //CCLOG("str %s",str);
        tmp->loadWithFile(str);
        LockPoolSingleton::Instance()->add(tmp);
    };
    srand(time(0));
    //random variable mas
    t = rand()%3 + 1;
    for (int i = 0; i < POOL_SIZE; i++) {
        if(t > 3)
            t = 1;
        mas[i] = t;
       // CCLOG("mas[i] = %d",t);
        t++;
    };
    for (int i = 0; i < POOL_SIZE; i++) {
        int a = rand()%POOL_SIZE,b = rand()%POOL_SIZE;
       // CCLOG("resort a %d b%d %d %d",a,b,mas[a],mas[b]);
        t = mas[a];
        mas[a] = mas[b];
        mas[b] = t;
    };
    
    //fill pool girl node
    for (int i = 0; i < COUNT_UNIQUE_POSE;++i)
    {
        BGirl *tmp = new BGirl;
        tmp->create(i+1);
        addChild(tmp->node,1);
        BraHelper::Instance()->num_girl = mas[i];
        m_girl[i] = tmp;
    };

};

void BGameScene::createAnimation()
{
    anim_girl = BComponent::create();
    if(BraHelper::Instance()->getGirlType() == 1)
        anim_girl->loadWithFile(BRA::loadImage("c_FinalAnimationReal.plist").c_str());
   else
        anim_girl->loadWithFile(BRA::loadImage("c_FinalAnimation.plist").c_str());
    anim_girl->setVisible(false);
    addChild(anim_girl,99);
};

void BGameScene::endGame()
{
    BraHelper::Instance()->setScore(count_lock_complite);
    CCDirector::sharedDirector()->pause();
    unscheduleUpdate();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("TimeEnd.caf");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
    //ObjCCalls::sendEventFlurry("session_end",false, "lock_count=%d",count_lock_complite);
    
    BraJoinSprite::clearAll();
    this->unscheduleAllSelectors();
    CCTextureCache::sharedTextureCache()->removeUnusedTextures();
    CCSpriteFrameCache::sharedSpriteFrameCache()->removeUnusedSpriteFrames();
    CCScene* pScene = EndGameScene::scene();
    CCDirector::sharedDirector()->replaceScene(pScene);
    CCDirector::sharedDirector()->resume();
};

void BGameScene::update(float _dt)
{
    char str[NAME_MAX];
    int min = (time_game)/60;
    sprintf(str, "%d:%.2d",min,int(time_game - min*60));
    label_time->setString(str);
    time_game -= _dt;
    if(time_game <= 0.0)
        endGame();
};

void BGameScene::hideNode(CCNode *_node)
{
    _node->setVisible(false);
};

void BGameScene::animationGirlPose()
{
    return;
    for (int i = 0; i < COUNT_VISIBLE_GIRL;++i)
    {
        CCActionInterval*  a1 = CCSkewBy::create(1,0.0 + (rand()%10/20.0 - 0.25),0.1 + rand()%5/10.0);
        CCAction*  action2 = CCRepeatForever::create((CCActionInterval*)(CCSequence::create((CCActionInterval*)(a1->copy()->autorelease()), a1->reverse(), NULL)));
        m_girl[i]->m_girl->hair->stopAllActions();
        m_girl[i]->m_girl->girl->runAction(action2);
        CCActionInterval*  a3 = CCSkewBy::create(3.0 + rand()%5/10.0,4.7 + rand()%10/10.0,2.3 + rand()%5/10.0);
        CCAction*  action3 = CCRepeatForever::create((CCActionInterval*)(CCSequence::create((CCActionInterval*)(a3->copy()->autorelease()), a3->reverse(), NULL)));
        m_girl[i]->m_girl->hair->runAction(action3);
    };
};

void BGameScene::next()
{
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("CameraMove.caf");//move sound
    BraHelper::Instance()->cleraHairList();
    char str[NAME_MAX];
    sprintf(str, "%d",count_lock_complite);
    label_count_lock->setString(str);
    m_parallax->next();
    //ObjCCalls::sendEventFlurry("unlock_bra",false, "lock_number=%d,unlock_time=%d",curent_layer_bra->getLockNumber(),time_start);
    
    CCAction *seq =CCSequence::create(CCMoveBy::create(BRA::TIME_ANIMATION_ZOOM, ccp(-win_size.width*1.5 + win_size.width*0.3,0)),NULL);
    anim_girl->runAction(seq);
    
    for (int i = 0; i < COUNT_UNIQUE_POSE;++i)
    {
        layer_bra[i]->end();
    };
    start_animation = false;
    LockPoolSingleton::Instance()->resort();
    animationGirlPose();
    resortGirlPose();    
    animateToPositionGirl();

    BraHelper::Instance()->previor_lock_id.clear();
};

//animation girl to start position
void BGameScene::animateToPositionGirl()
{
    int i = 0;
    while (i < COUNT_UNIQUE_POSE) {
        m_girl[i]->node->setScale(BASE_LAYER_SCALE);
        m_girl[i]->node->stopAllActions();
        m_girl[i]->node->setPosition(ccp(win_size.width*2.1 + win_size.width*0.3*i,win_size.height*0.4));
        if(COUNT_VISIBLE_GIRL == 3)
        {
            m_girl[i]->node->runAction(CCMoveTo::create(BRA::TIME_ANIMATION_ZOOM, ccp(win_size.width*0.1 + win_size.width*0.3*i,win_size.height*0.4)));
        }else if(COUNT_VISIBLE_GIRL == 2)
        {
            m_girl[i]->node->runAction(CCMoveTo::create(BRA::TIME_ANIMATION_ZOOM, ccp(win_size.width*0.2 + win_size.width*0.4*i,win_size.height*0.4)));
        };
        m_girl[i]->node->setVisible(i < COUNT_VISIBLE_GIRL);
        m_girl[i]->reset();
        layer_bra[i]->reset();
        layer_bra[i]->removeFromParentAndCleanup(false);
        m_girl[i]->node->addChild(layer_bra[i],8);
        ++i;
    };
    generatedBraClothTexture();
};

void BGameScene::createMenu()
{
    m_gui = GUILayer::create();
    m_gui->retain();
    addChild(m_gui,9);
    m_gui->loadWithFile(BRA::loadImage("c_GameScene.plist").c_str());
    
    m_gui->setTarget(this);
    m_gui->addFuncToMap("menuCall", callfuncND_selector(BGameScene::callMenu));
    m_gui->setTouchEnabled(true);
    //m_gui->setPositionX(-(1024-win_size.width)*0.1);
    m_gui->setPosition(ccp(win_size.width*0.5,win_size.height*0.5));
    label_time = static_cast<CCLabelTTF *>(m_gui->getChildByTag(52));
    label_count_lock = static_cast<CCLabelTTF *>(m_gui->getChildByTag(51));
    
    //skip button
    btn_skip = m_gui->getChildByTag(8);
    btn_skip_start_pos = btn_skip->getPosition();
    btn_skip->setPositionX(btn_skip_start_pos.x-btn_skip->getContentSize().width*3);
    btn_menu = m_gui->getChildByTag(6);
    btn_menu_start_pos = btn_menu->getPosition();
};

void BGameScene::lockSkip(void *_data)
{
    this->stopAllActions();
    BraHelper::Instance()->cleraHairList();
    m_parallax->node->setVisible(true);
    this->setTouchEnabled(true);
        
    btn_skip->runAction(CCMoveTo::create(TIME_ANIMATION_BTN, ccp(btn_skip_start_pos.x-btn_skip->getContentSize().width*3,btn_skip->getPositionY())));
    btn_menu->runAction(CCMoveTo::create(TIME_ANIMATION_BTN, btn_menu_start_pos));
    
    
    //end time
    timeval t;
    gettimeofday(&t,NULL);
    time_start = t.tv_sec - time_start; //time unlock the lock
    //ObjCCalls::sendEventFlurry("lock_skip",false, "lock_number=%d,skip_time=%d",curent_layer_bra->getLockNumber(),time_start);
    
    if(time_start <= TIME_FIRS_UNLOCK)
    {
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("BraUnlockedFast.caf");
    }
    else
    {
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("BraUnlocked.caf");
    }
    
    for (int i = 0; i < COUNT_UNIQUE_POSE;++i)
    {
        layer_bra[i]->end();
    };
    
    for(int j = 0;j < COUNT_UNIQUE_POSE;j++)
    {
        m_girl[j]->node->stopAllActions();
        if(layer_bra[j] == _data)
        {
            unlock_girl = m_girl[j];
            unlock_girl->back_bra->setVisible(false);
            unlock_girl->back_bra->setPosition(ccp(win_size.width*0.5,win_size.height*0.5));
        };
    };
    unlock_girl->m_girl->hair->setPositionY(unlock_girl->m_girl->hair->getPositionY() - HAIR_ANIM_UP);

    animationGirlPose();
    
    CCAction * move_ = CCMoveTo::create(BRA::TIME_ANIMATION_ZOOM, unlock_girl->start_position);
    CCActionInterval* move_ease3 = CCEaseSineInOut::create((CCActionInterval*)(move_->copy()->autorelease()) );
    CCAction * scale_ = CCScaleTo::create(BRA::TIME_ANIMATION_ZOOM, BASE_LAYER_SCALE);
    CCActionInterval* zoom_ease3 = CCEaseSineInOut::create((CCActionInterval*)(scale_->copy()->autorelease()));
    
    CCAction *seq_layer = CCSpawn::create(zoom_ease3,move_ease3,NULL);
    unlock_girl->node->runAction(seq_layer);

    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("CameraMove.caf");//move sound
    
    char str[NAME_MAX];
    sprintf(str, "%d",count_lock_complite);
    label_count_lock->setString(str);
    m_parallax->next();
   // ObjCCalls::sendEventFlurry("unlock_bra",false, "lock_number=%d,unlock_time=%d",curent_layer_bra->getLockNumber(),time_start);
    
    start_animation = false;
    LockPoolSingleton::Instance()->resort();
    
    resortGirlPose();
    animateToPositionGirl();
    BraHelper::Instance()->previor_lock_id.clear();
};
void BGameScene::resortGirlPose()
{
    //resort not pool
    for (int i = 0; i < 10;++i)
    {
        int k = rand()%3,l = rand()%3;
        if(k != l)
        {
            BGirl *tmp = m_girl[k];
            m_girl[k] = m_girl[l];
            m_girl[l] = tmp;
        };
    };
};
void BGameScene::lockComplite(CCNode* d, void *_data)
{
    m_parallax->node->setVisible(true);
    this->setTouchEnabled(true);
    count_lock_complite++;
    CCTextureCache::sharedTextureCache()->removeUnusedTextures();
    
    btn_skip->runAction(CCMoveTo::create(TIME_ANIMATION_BTN, ccp(btn_skip_start_pos.x-btn_skip->getContentSize().width*3,btn_skip->getPositionY())));
    btn_menu->runAction(CCMoveTo::create(TIME_ANIMATION_BTN, btn_menu_start_pos));
    //end time
    timeval t;
    gettimeofday(&t,NULL);
    time_start = t.tv_sec - time_start; //time unlock the lock
    
    if(time_start <= TIME_FIRS_UNLOCK)
    {
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("BraUnlockedFast.caf");
    }
    else
    {
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("BraUnlocked.caf");
    }
    //CCLOG("lockComplite %2d %.2d",_data,this);
    //BGirl *p_girl = NULL;
    
    for(int j = 0;j < COUNT_VISIBLE_GIRL;j++)
    {
        if(layer_bra[j] == _data)
        {
           // unlock_girl = m_girl[j];
            unlock_girl->back_bra->setVisible(true);
            unlock_girl->back_bra->setPosition(ccp(win_size.width*0.5,win_size.height*0.5));
        };
    };
    CCAction * move_ = CCMoveTo::create(BRA::TIME_ANIMATION_ZOOM, unlock_girl->start_position);
    CCActionInterval* move_ease3 = CCEaseSineInOut::create((CCActionInterval*)(move_->copy()->autorelease()) );
    CCAction * scale_ = CCScaleTo::create(BRA::TIME_ANIMATION_ZOOM, BASE_LAYER_SCALE);
    CCActionInterval* zoom_ease3 = CCEaseSineInOut::create((CCActionInterval*)(scale_->copy()->autorelease()));
    CCAction *seq_layer = CCSpawn::create(zoom_ease3,move_ease3,NULL);
    unlock_girl->node->runAction(seq_layer);
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("Animation1.mp3");
    unlock_girl->m_girl->hair->runAction(CCMoveBy::create(BRA::TIME_ANIMATION_ZOOM*0.75, ccp(0,-HAIR_ANIM_UP)));
    CCAction *spa = CCSequence::create(CCDelayTime::create(BRA::TIME_ANIMATION_ZOOM + BRA::TIME_ANIMATION_ZOOM - 0.1),CCCallFunc::create(this, callfunc_selector(BGameScene::startAnimGirl1)),NULL);
    
    this->runAction(spa);
};

void BGameScene::startAnimGirl1()
{
    CCPoint p = unlock_girl->m_girl->m_pose->convertToWorldSpaceAR(unlock_girl->m_girl->girl->getPosition());
    
    anim_girl->setPosition(p);
    if(rand()%2 == 0)
    {
        anim_girl->setScaleX(-1);
    }
    else
    {
        anim_girl->setScaleX(1);
    };
    
    anim_girl->getChildByTag(2)->setVisible(false);
    anim_girl->getChildByTag(3)->setVisible(false);
    anim_girl->getChildByTag(4)->setVisible(false);
    unlock_girl->node->setVisible(false);
    anim_girl->setVisible(true);
    anim_girl->getChildByTag(1)->setVisible(true);
    char str[NAME_MAX];
    sprintf(str, "%sAnimation.png",BRA::getFileWithoutSuffic(unlock_girl->m_girl->hair->getFileName()).c_str());
    CCLOG("HAIR %s oldName %s %d",str,unlock_girl->m_girl->hair->getFileName().c_str(),unlock_girl->m_girl->hair_num);
    CCSpriteFrame * sframe = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);
    if(sframe)
        NodeToClip(anim_girl->getChildByTag(2))->setTextureRect(sframe->getRect(),sframe->isRotated(),sframe->getOriginalSize());
    sprintf(str, "%sAnimation.png",BRA::getFileWithoutSuffic(unlock_girl->m_girl->bikini->getFileName()).c_str());
    sframe = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);
    if(sframe)
        NodeToClip(anim_girl->getChildByTag(3))->setTextureRect(sframe->getRect(),sframe->isRotated(),sframe->getOriginalSize());
    
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("Animation2.mp3");

    NodeToClip(anim_girl->getChildByTag(1))->setState(unlock_girl->m_girl->girl_num);

    CCAction *spa = CCSequence::create(CCDelayTime::create(0.15),CCCallFunc::create(this, callfunc_selector(BGameScene::startAnimGirl2)),NULL);
    this->runAction(spa);
};

void BGameScene::startAnimGirl2()
{
  //  CCDirector::sharedDirector()->pause();
    anim_girl->getChildByTag(2)->setVisible(true);
    anim_girl->getChildByTag(3)->setVisible(true);
    anim_girl->getChildByTag(4)->setVisible(true);
    anim_girl->getChildByTag(1)->setVisible(false);
    NodeToClip(anim_girl->getChildByTag(4))->setState(unlock_girl->m_girl->girl_num);
   // CCLayer::visit();
  //  CCDirector::sharedDirector()->resume();
    CCAction *spa = CCSequence::create(CCDelayTime::create(0.2),CCCallFunc::create(this, callfunc_selector(BGameScene::next)),NULL);
    this->runAction(spa);
};


void BGameScene::ccTouchesEnded(CCSet* touches, CCEvent* event)
{
    //Add a new body/atlas sprite at the touched location
    CCSetIterator it;
    CCTouch* touch;
    for( it = touches->begin(); it != touches->end(); it++) 
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        m_gui->setTouchEnd(location);
    }
}

void BGameScene::ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    if(end_animation)
        return;
    CCSetIterator it;
    CCTouch* touch;
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
       // CCLOG("ccTouchesBegan:: %d",touch->getID());
        if(!touch)
            break;

        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        CCLOG("GUILayer::ccTouchesBegan");
        m_gui->setTouchBegan(location);
        if(!start_animation)
        {
            if(m_parallax->touches->setTouchBegan(location))
            {
                m_parallax->touches->setTouchEnd(location);
                break;
            };
            
            curent_girl_num = -1;
            for(int i = 0;i < COUNT_VISIBLE_GIRL; ++i)
            {
               // if(m_girl[i]->is_unlock)
               //     continue;
                CCPoint tpoint  = m_girl[i]->node->convertToNodeSpace(location);
                if(fabs(tpoint.x - win_size.width*0.5) < win_size.width&& fabs(tpoint.y) < (win_size.height*2))
                {
                    start_animation = true;
                    //animation zoom to unlock of the lock
                    CCPoint pos_lock = layer_bra[i]->getLockStartPosition();
                    float koef = (win_size.width == 1024)?1.0:0.6;
                    float _h = (1.0 - pos_lock.y) - 0.5/BraHelper::Instance()->camera_scale*koef;
                    float _w = (1.0 - pos_lock.x) - 0.5/BraHelper::Instance()->camera_scale*koef;
                    
                    m_girl[i]->start_position =  m_girl[1]->node->getPosition();
                    if(BraHelper::Instance()->getGirlType() == 1)//realistic
                    {
                        m_girl[i]->start_position.x =  m_girl[0]->node->getPositionX() + (m_girl[1]->node->getPositionX() - m_girl[0]->node->getPositionX())/2;
                        m_girl[i]->start_position.y -=20;
                    }
                    m_girl[i]->m_girl->girl->stopAllActions();
                    CCAction *seq = CCSpawn::create(CCScaleTo::create(BRA::TIME_ANIMATION_ZOOM, 1.0/BraHelper::Instance()->camera_scale*koef),CCMoveTo::create(BRA::TIME_ANIMATION_ZOOM, ccp(win_size.width*_w,win_size.height*_h)),CCSkewTo::create(BRA::TIME_ANIMATION_ZOOM, 0, 0),NULL);
                    m_girl[i]->node->runAction(seq);
                    m_girl[i]->m_girl->hair->runAction(CCMoveBy::create(BRA::TIME_ANIMATION_ZOOM, ccp(0,HAIR_ANIM_UP)));
                    m_girl[i]->is_unlock = false;
                    curent_girl_num = m_girl[i]->m_girl->num;
                    
                    //activeted lock
                    layer_bra[i]->start();
                    curent_layer_bra = layer_bra[i];
                    BraHelper::Instance()->curent_girl = m_girl[i];
                    //send girl to upper layer
                    reorderChild(m_girl[i]->node, 2);
                    
                    //start time for unlock
                    timeval t;
                    gettimeofday(&t,NULL);
                    time_start = t.tv_sec;
                    
                    //send statistic change girl to flurry
                    std::string event_str = "girl_selected";
                    if(BraHelper::Instance()->getGirlType() == 1) //realistic girl
                    {
                        event_str = "girl_selected_realistic";
                    };
                    unlock_girl = m_girl[i];
                    //ObjCCalls::sendEventFlurry(event_str.c_str(),false, "body_number=%d,hair_number=%d,lock_number=%d,position_number=%d",m_girl[i]->m_girl->girl_num,m_girl[i]->m_girl->hair_num,layer_bra[i]->getLockNumber(),i);
                    event_str = "girl_non_selected";
                    if(BraHelper::Instance()->getGirlType() == 1) //realistic girl
                    {
                        event_str = "girl_non_selected_realistic";
                    };
                    
                    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("Zoom.mp3");
                    
                    
                    //hide non select girl
                    for(int j = 0;j < COUNT_UNIQUE_POSE; ++j)
                    {
                        if(m_girl[j]->m_girl->num != curent_girl_num)
                        {
                            m_girl[j]->m_girl->girl->stopAllActions();
                            layer_bra[j]->end();
                            CCAction * seq = CCSequence::create(CCDelayTime::create(BRA::TIME_ANIMATION_ZOOM),CCCallFuncN::create(m_girl[j]->node, callfuncN_selector(BGameScene::hideNode)), NULL);
                            m_girl[j]->node->runAction(seq);
                            //ObjCCalls::sendEventFlurry(event_str.c_str(),false, "body_number=%d,hair_number=%d,lock_number=%d",m_girl[j]->m_girl->girl_num,m_girl[j]->m_girl->hair_num,layer_bra[j]->getLockNumber());
                        }
                    };
                    
                    btn_skip->runAction(CCMoveTo::create(TIME_ANIMATION_BTN, btn_skip_start_pos));
                    btn_menu->runAction(CCMoveTo::create(TIME_ANIMATION_BTN, ccp(btn_menu_start_pos.x-btn_menu->getContentSize().width*3,btn_menu->getPositionY())));
                    
                    this->runAction(CCSequence::create(CCDelayTime::create(BRA::TIME_ANIMATION_ZOOM*2),CCCallFunc::create(this, callfunc_selector(BGameScene::unloadTexture)) ,NULL));
                    return;
                };
            }
        }
    };
};

void BGameScene::unloadTexture()
{
  //  m_parallax->node->setVisible(false);
    btn_skip->runAction(CCMoveTo::create(TIME_ANIMATION_BTN, btn_skip_start_pos));
    btn_menu->runAction(CCMoveTo::create(TIME_ANIMATION_BTN, ccp(btn_menu_start_pos.x-btn_menu->getContentSize().width*3,btn_menu->getPositionY())));
};

void BGameScene::ccTouchesMoved(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    CCSetIterator it;
    CCTouch* touch;
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        if(!touch)
            break;
    };
};

void BGameScene::callMenu(CCNode *_sender,void *_d)
{
    if(_d)
    {
        if(*static_cast<int*>(_d) == BRA::actionMENU)
        {
            CCDirector::sharedDirector()->pause();

            BraJoinSprite::clearAll();
            this->unscheduleAllSelectors();
            CCTextureCache::sharedTextureCache()->removeUnusedTextures();
            CCSpriteFrameCache::sharedSpriteFrameCache()->removeUnusedSpriteFrames();

            
            CCScene* pScene = MenuScene::scene();
            CCDirector::sharedDirector()->replaceScene(pScene);
            CCDirector::sharedDirector()->resume();

            return;
        };
        
        if(*static_cast<int*>(_d) == 7)
        {
            lockSkip(curent_layer_bra);
            return;
        };
        
    };
    
    if(_sender->getTag() == 2)
    {
        next();
    }
    else
    {
        BraJoinSprite::clearAll();
        this->unscheduleAllSelectors();
        CCScene* pScene = BGameScene::scene();
        CCDirector::sharedDirector()->replaceScene(pScene);
    }
};

void BGameScene::draw()
{
    //
    // IMPORTANT:
    // This is only for debug purposes
    // It is recommend to disable it
    //
#if DEBUG_MODE
    CCLayer::draw();
    
#else
#if SHOW_BODY_OBJECT
    CCLayer::draw();
    
    ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
    
    kmGLPushMatrix();
    
    world->DrawDebugData();
    
    kmGLPopMatrix();
    CHECK_GL_ERROR_DEBUG();
#endif
#endif
};

CCScene* BGameScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // add layer as a child to scene
    CCLayer* layer = new BGameScene();
    scene->addChild(layer);
    layer->release();
    return scene;
}
