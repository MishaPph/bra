//
//  HelloWorldScene.h
//  Bra
//
//  Created by PR1.Stigol on 23.01.13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//
#ifndef __HELLO_WORLD_H__
#define __HELLO_WORLD_H__

// When you import this file, you import all the cocos2d classes
#include "cocos2d.h"
#include "BRA.h"
#include "BSpriteBody.h"
#include "LayerBra.h"
#include "GuiLayer.h"
#include "BraHelper.h"
#include "BGirlPose.h"

using namespace cocos2d;
static const int POOL_SIZE = 10;


class BParallax
{
    CCSprite * sp[4];
    float last_x;
public:
    cocos2d::CCNode * node;
    GUILayer *touches;
    void create();
    void next();
    ~BParallax();
};


struct BGirl
{
    cocos2d::CCSize win_size;
    cocos2d::CCPoint start_position;
   
    BGirlPose* m_girl;
    cocos2d::CCSprite* back_bra;
    cocos2d::CCNode * node;
    bool is_unlock;
   // BaseLayerLock* layer_bra;
    void create(const int &_pose);
    BGirl()
    {
      //  layer_bra = NULL;
        node = NULL;
        m_girl = NULL;
        back_bra = NULL;
        is_unlock = false;
    }
    void reset();
    ~BGirl();
};


class BGameScene : public cocos2d::CCLayer {
public:
    ~BGameScene();
    BGameScene();
    // returns a Scene that contains the HelloWorld as the only child
    long time_start;
    int count_lock_complite;
    CCLabelTTF *label_time,*label_count_lock;
    float time_game;
    void lockComplite(cocos2d::CCNode* d, void *_data);
    void lockSkip(void *_data);
    static cocos2d::CCScene* scene();
    void createMenu();
    void createPoolObject();
    void next();
    void update(float _dt);
    void startAnimGirl1();
    void startAnimGirl2();
    void unloadTexture();
    void createAnimation();
    void animateToPositionGirl();
    void resortGirlPose();
    void endGame();
    void hideNode(CCNode *_node);
    virtual void draw();
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    void animationGirlPose();
    void createLayerLock();
    void createParallax();
    void createBGirl();
    void generatedBraClothTexture();
private:
    int COUNT_VISIBLE_GIRL;
    int COUNT_UNIQUE_POSE;
    //BaseLayerLock* layer_bra;
    BaseLayerLock* layer_bra[3];
    BaseLayerLock* curent_layer_bra;
    //CCNode * girl_node[3];
    BGirl *unlock_girl;
    int curent_girl_num;
    BComponent * anim_girl;
    CCMenuItem *menuitem;
    BGirl *m_girl[3];
    cocos2d::CCSize win_size;
    bool start_animation,end_animation,end_game;
    void callMenu(CCNode *_sender,void *_d = NULL);
    void createCocoa(CCNode *_sender,void *_d = NULL);
    void createGirl(int _id);
    GUILayer *m_gui;
    CCSprite* bg;
    CCNode* btn_skip;
    CCPoint btn_skip_start_pos;
    CCNode* btn_menu;
    CCPoint btn_menu_start_pos;
    BParallax *m_parallax;
   // std::list<CCTexture2D *> clear_texture;
};

#endif // __HELLO_WORLD_H__
