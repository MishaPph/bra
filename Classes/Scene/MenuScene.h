//
//  MenuScene.h
//  Bra
//
//  Created by PR1.Stigol on 13.03.13.
//
//

#ifndef __Bra__MenuScene__
#define __Bra__MenuScene__

#include <iostream>
#include "cocos2d.h"
#include "GuiLayer.h"
#include "BGameScene.h"
using namespace cocos2d;

class MenuScene : public cocos2d::CCLayer {
public:
    ~MenuScene();
    MenuScene();
    static cocos2d::CCScene* scene();
    virtual void draw();
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    void callMenu(CCNode *_sender,void *_d);
    void selectGirl(CCNode *_sender,void *_d);
    void touchGirl(CCNode *_sender,void *_d);
    void animFrame1Cart();
    void animFrame2Cart();
    void animFrame3Cart();
    void animNewGirlCart();
    void createCartoon();
    
    void createReal();
    void animFrame1Real();
    void animFrame2Real();
    void animFrame3Real();
    void animNewGirlReal();
private:
    bool is_start_play;
    GUILayer *m_gui;
    CCSize win_size;
    CCTexture2D *tex;
    
    BGirlPose* m_girl;
    BGirlPose* m_girl_cartoon[3];
    BComponent *anim_girl_cartoon;
    CCNode * node_cart;
    
    BGirlPose* m_girl_r;
    BGirlPose* m_girl_real[3];
    BComponent *anim_girl_real;
    CCNode * node_real;
    CCSprite * border_cartoon;
    CCSprite * border_real;
};

class TestScene : public cocos2d::CCLayer {
public:
    ~TestScene(){};
    TestScene();
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    static cocos2d::CCScene* scene();
private:
    CCSprite * m_sprite;
};
#endif /* defined(__Bra__MenuScene__) */
