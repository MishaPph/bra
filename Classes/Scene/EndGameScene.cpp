//
//  EndGameScene.cpp
//  Bra
//
//  Created by PR1.Stigol on 13.03.13.
//
//

#include "EndGameScene.h"
#include "BGameScene.h"
#include "SimpleAudioEngine.h"
#include "Component\BElementButton.h"
#include "Component\BElementClip.h"

const float TIME_FRAME = 0.2f;

CCScene* EndGameScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    // add layer as a child to scene
    CCLayer* layer = new EndGameScene();
    CCLOG("scene:: scene %d layer %d ",scene,layer);
    if(layer->init())
    {
        scene->addChild(layer);
        layer->setTouchEnabled(true);
        layer->release();
    };
    return scene;
};

EndGameScene::~EndGameScene()
{
    CCLOG("~EndGameScene:: destroy");
    removeAllChildrenWithCleanup(true);
    anim_girl_cartoon = NULL;
    anim_girl_real = NULL;
    for (int i = 0; i < 3 ; ++i) {
        delete m_girl_real[i];
    }
};

#pragma mark - INIT


EndGameScene::EndGameScene()
{
    is_start_play = false;
    this->setTouchEnabled(false);
    win_size = CCDirector::sharedDirector()->getWinSize();
#ifndef DISABLE_BACKGROUND_SOUND
    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("Menu Theme.mp3",true);
#endif
    
    m_gui = GUILayer::create();
    addChild(m_gui);
    
    m_gui->loadWithFile(BRA::loadImage("c_End.plist").c_str());
    m_gui->setTarget(this);
    
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(BRA::loadImage("a_AnimationReal.plist").c_str());
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(BRA::loadImage("a_HairReal.plist").c_str());
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(BRA::loadImage("a_Hair.plist").c_str());
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(BRA::loadImage("a_Animation.plist").c_str());
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(BRA::loadImage("a_Pose1.plist").c_str());
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(BRA::loadImage("a_Pose2.plist").c_str());
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(BRA::loadImage("a_Pose3.plist").c_str());
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(BRA::loadImage("a_PoseReal1.plist").c_str());
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(BRA::loadImage("a_PoseReal2.plist").c_str());
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(BRA::loadImage("a_PoseReal3.plist").c_str());
    
    //  CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(BRA::loadImage("a_Animation.plist").c_str());
    
    m_gui->addFuncToMap("menuCall", callfuncND_selector(EndGameScene::callMenu));
    m_gui->addFuncToMap("selectGirl", callfuncND_selector(EndGameScene::selectGirl));
    m_gui->addFuncToMap("touchGirl", callfuncND_selector(EndGameScene::touchGirl));
    char str[NAME_MAX];
    sprintf(str, "%d",BraHelper::Instance()->getScore());
    static_cast<CCLabelTTF*>(m_gui->getChildByTag(51))->setString(str);
    //  static_cast<BElementCheckBox *>(m_gui->getChildByTag(15))->setChecked(BraHelper::Instance()->isRecordVideo());
    m_gui->setTouchEnabled(true);
    m_gui->setPosition(ccp(win_size.width*0.5,win_size.height*0.5));
    
    this->setTouchEnabled(true);
    this->setVisible(true);
    
    createCartoon();
    createReal();
    border_cartoon = CCSprite::createWithSpriteFrameName("RBtnGirlOff.png");
    border_cartoon->setPosition(ccpAdd(m_gui->getPosition(),m_gui->getChildByTag(19)->getPosition()));
    addChild(border_cartoon,100);
    border_real = CCSprite::createWithSpriteFrameName("RBtnGirlRealOff.png");
    border_real->setPosition(ccpAdd(m_gui->getPosition(),m_gui->getChildByTag(11)->getPosition()));
    addChild(border_real,100);
    CCTextureCache::sharedTextureCache()->dumpCachedTextureInfo();    
};

void EndGameScene::createReal()
{
    anim_girl_real = BComponent::create();
    BraHelper::Instance()->setGirlType(1);
    anim_girl_real->loadWithFile(BRA::loadImage("c_FinalAnimationReal.plist").c_str());
    node_real = CCNode::create();
    
    anim_girl_real->setVisible(false);
    node_real->addChild(anim_girl_real,99);
    addChild(node_real,2);
    srand(time(0));
    
    for (int i = 0; i < 3 ; ++i) {
        m_girl_real[i] = new BGirlPose;
        m_girl_real[i]->setType(1);
        m_girl_real[i]->create(i + 1);
        m_girl_real[i]->m_pose->setVisible(false);
        node_real->addChild(m_girl_real[i]->m_pose,98);
    }
    m_girl_r = m_girl_real[rand()%3];
    anim_girl_real->setPosition(m_girl_r->m_pose->getPosition());
    node_real->setPosition(ccp(-win_size.width*0.05,win_size.height*0.13));
    if(win_size.width == 568)
    {
        node_real->setPositionX(0);
    }
    node_real->setScale(0.65f);
    animFrame1Real();
};

void EndGameScene::createCartoon()
{
    anim_girl_cartoon = BComponent::create();
    BraHelper::Instance()->setGirlType(2);
    
    anim_girl_cartoon->loadWithFile(BRA::loadImage("c_FinalAnimation.plist").c_str());
    
    node_cart = CCNode::create();
    anim_girl_cartoon->setVisible(false);
    node_cart->addChild(anim_girl_cartoon,99);
    addChild(node_cart,2);
    srand(time(0));
    
    for (int i = 0; i < 3 ; ++i) {
        m_girl_cartoon[i] = new BGirlPose;
        m_girl_cartoon[i]->setType(2);
        m_girl_cartoon[i]->create(i + 1);
        m_girl_cartoon[i]->m_pose->setVisible(false);
        node_cart->addChild(m_girl_cartoon[i]->m_pose,98);
    }
    m_girl = m_girl_cartoon[rand()%3];
    anim_girl_cartoon->setPosition(m_girl->m_pose->getPosition());
    node_cart->setPosition(ccp(win_size.width*0.4,win_size.height*0.13));
    if(win_size.width == 568)
    {
        node_cart->setPositionX(win_size.width*0.34);
    }
    node_cart->setScale(0.65f);
    animFrame1Cart();
};

void EndGameScene::animFrame1Cart()
{
    BraHelper::Instance()->cleraHairList();
    anim_girl_cartoon->setVisible(false);
    m_girl = m_girl_cartoon[rand()%3];
    m_girl->randHair();
    m_girl->randBikini();
    m_girl->randGirl();
    m_girl->m_pose->setVisible(true);
    CCAction *spa = CCSequence::create(CCDelayTime::create(TIME_FRAME),CCCallFunc::create(this, callfunc_selector(EndGameScene::animFrame2Cart)),NULL);
    node_cart->runAction(spa);
};

void EndGameScene::touchGirl(CCNode *_sender,void *_d)
{
    switch(*static_cast<int*>(_d))
    {
        case 1:
        {
            for (int i = 0; i < 3 ; ++i) {
                m_girl_real[i]->m_pose->setVisible(false);
            }
            anim_girl_real->setVisible(false);
            CCSpriteFrame* sframe = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("RBtnGirlOff.png");
            if(sframe)
                border_cartoon->setTextureRect(sframe->getRect(),sframe->isRotated(),sframe->getOriginalSize());
            
            sframe = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("RBtnGirlRealOn.png");
            if(sframe)
                border_real->setTextureRect(sframe->getRect(),sframe->isRotated(),sframe->getOriginalSize());
            
            node_real->stopAllActions();
            animFrame1Real();
        };
            break;
        case 2:
        {
            for (int i = 0; i < 3 ; ++i) {
                m_girl_cartoon[i]->m_pose->setVisible(false);
            }
            anim_girl_cartoon->setVisible(false);
            CCSpriteFrame* sframe = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("RBtnGirlOn.png");
            if(sframe)
                border_cartoon->setTextureRect(sframe->getRect(),sframe->isRotated(),sframe->getOriginalSize());
            
            sframe = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("RBtnGirlRealOff.png");
            if(sframe)
                border_real->setTextureRect(sframe->getRect(),sframe->isRotated(),sframe->getOriginalSize());
            
            node_cart->stopAllActions();
            animFrame1Cart();
        };
            break;
        default:
            break;
            //CCAssert(0, "Uknow tag");
    };
};

void EndGameScene::animFrame2Cart()
{
    if(rand()%2 == 0)
    {
        anim_girl_cartoon->setScaleX(-1);
    }
    else
    {
        anim_girl_cartoon->setScaleX(1);
    };
    
    anim_girl_cartoon->getChildByTag(2)->setVisible(false);
    anim_girl_cartoon->getChildByTag(3)->setVisible(false);
    anim_girl_cartoon->getChildByTag(4)->setVisible(false);
    m_girl->m_pose->setVisible(false);
    anim_girl_cartoon->setVisible(true);
    anim_girl_cartoon->getChildByTag(1)->setVisible(true);
    char str[NAME_MAX];
    sprintf(str, "%sAnimation.png",BRA::getFileWithoutSuffic(m_girl->hair->getFileName()).c_str());
    CCSpriteFrame * sframe = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);
    if(sframe)
        NodeToClip(anim_girl_cartoon->getChildByTag(2))->setTextureRect(sframe->getRect(),sframe->isRotated(),sframe->getOriginalSize());
    sprintf(str, "%sAnimation.png",BRA::getFileWithoutSuffic(m_girl->bikini->getFileName()).c_str());
    sframe = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);
    if(sframe)
        NodeToClip(anim_girl_cartoon->getChildByTag(3))->setTextureRect(sframe->getRect(),sframe->isRotated(),sframe->getOriginalSize());
    
    NodeToClip(anim_girl_cartoon->getChildByTag(1))->setState(m_girl->girl_num);
    
    CCAction *spa = CCSequence::create(CCDelayTime::create(TIME_FRAME),CCCallFunc::create(this, callfunc_selector(EndGameScene::animFrame3Cart)),NULL);
    node_cart->runAction(spa);
};

void EndGameScene::animFrame3Cart()
{
    anim_girl_cartoon->getChildByTag(2)->setVisible(true);
    anim_girl_cartoon->getChildByTag(3)->setVisible(true);
    anim_girl_cartoon->getChildByTag(4)->setVisible(true);
    anim_girl_cartoon->getChildByTag(1)->setVisible(false);
    NodeToClip(anim_girl_cartoon->getChildByTag(4))->setState(m_girl->girl_num);
    CCAction *spa = CCSequence::create(CCDelayTime::create(TIME_FRAME),CCCallFunc::create(this, callfunc_selector(EndGameScene::animNewGirlCart)),NULL);
    node_cart->runAction(spa);
};

void EndGameScene::animNewGirlCart()
{
    CCAction *spa = CCSequence::create(CCDelayTime::create(5.0),CCCallFunc::create(this, callfunc_selector(EndGameScene::animFrame1Cart)),NULL);
    node_cart->runAction(spa);
};

void EndGameScene::animFrame1Real()
{
    BraHelper::Instance()->cleraHairList();
    anim_girl_real->setVisible(false);
    m_girl_r = m_girl_real[rand()%3];
    m_girl_r->randHair();
    m_girl_r->randBikini();
    m_girl_r->randGirl();
    m_girl_r->m_pose->setVisible(true);
    CCAction *spa = CCSequence::create(CCDelayTime::create(TIME_FRAME),CCCallFunc::create(this, callfunc_selector(EndGameScene::animFrame2Real)),NULL);
    node_real->runAction(spa);
};

void EndGameScene::animFrame2Real()
{
    if(rand()%2 == 0)
    {
        anim_girl_real->setScaleX(-1);
    }
    else
    {
        anim_girl_real->setScaleX(1);
    };
    
    anim_girl_real->getChildByTag(2)->setVisible(false);
    anim_girl_real->getChildByTag(3)->setVisible(false);
    anim_girl_real->getChildByTag(4)->setVisible(false);
    m_girl_r->m_pose->setVisible(false);
    anim_girl_real->setVisible(true);
    anim_girl_real->getChildByTag(1)->setVisible(true);
    char str[NAME_MAX];
    sprintf(str, "%sAnimation.png",BRA::getFileWithoutSuffic(m_girl_r->hair->getFileName()).c_str());
    CCSpriteFrame * sframe = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);
    if(sframe)
        NodeToClip(anim_girl_real->getChildByTag(2))->setTextureRect(sframe->getRect(),sframe->isRotated(),sframe->getOriginalSize());
    sprintf(str, "%sAnimation.png",BRA::getFileWithoutSuffic(m_girl_r->bikini->getFileName()).c_str());
    sframe = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);
    if(sframe)
        NodeToClip(anim_girl_real->getChildByTag(3))->setTextureRect(sframe->getRect(),sframe->isRotated(),sframe->getOriginalSize());
    
    NodeToClip(anim_girl_real->getChildByTag(1))->setState(m_girl_r->girl_num);
    
    CCAction *spa = CCSequence::create(CCDelayTime::create(TIME_FRAME),CCCallFunc::create(this, callfunc_selector(EndGameScene::animFrame3Real)),NULL);
    node_real->runAction(spa);
};

void EndGameScene::animFrame3Real()
{
    anim_girl_real->getChildByTag(2)->setVisible(true);
    anim_girl_real->getChildByTag(3)->setVisible(true);
    anim_girl_real->getChildByTag(4)->setVisible(true);
    anim_girl_real->getChildByTag(1)->setVisible(false);
    NodeToClip(anim_girl_real->getChildByTag(4))->setState(m_girl_r->girl_num);
    CCAction *spa = CCSequence::create(CCDelayTime::create(TIME_FRAME),CCCallFunc::create(this, callfunc_selector(EndGameScene::animNewGirlReal)),NULL);
    node_real->runAction(spa);
};

void EndGameScene::animNewGirlReal()
{
    CCAction *spa = CCSequence::create(CCDelayTime::create(5.0),CCCallFunc::create(this, callfunc_selector(EndGameScene::animFrame1Real)),NULL);
    node_real->runAction(spa);
};


void EndGameScene::selectGirl(CCNode *_sender,void *_value)
{
    switch(*static_cast<int*>(_value))
    {
        case 1:
        {
            // static_cast<BElementRadioButton *>(m_gui->getChildByTag(21))->setChecked(true);
            //  static_cast<BElementRadioButton *>(m_gui->getChildByTag(22))->setChecked(false);
            BraHelper::Instance()->setGirlType(1);
            int d = 1;
            callMenu(NULL,&d);
        };
            break;
        case 2:
        {
            //  static_cast<BElementRadioButton *>(m_gui->getChildByTag(21))->setChecked(false);
            //  static_cast<BElementRadioButton *>(m_gui->getChildByTag(22))->setChecked(true);
            BraHelper::Instance()->setGirlType(2);
            int d = 1;
            callMenu(NULL,&d);
        };
            break;
        default:
            break;
            //CCAssert(0, "Uknow tag");
    };
};

void EndGameScene::callMenu(CCNode* _sender,void *_value)
{
    if(is_start_play)
        return;
    // CCLOG("_value %d %d node_tag %d" ,*static_cast<int*>(_value),static_cast<int*>(_value),_sender->getTag());
    stopAllActions();
    switch(*static_cast<int*>(_value))
    {
        case BRA::actionPLAY:
        {
            is_start_play = true;
            this->setTouchEnabled(false);
            unscheduleUpdate();
            //  BraHelper::Instance()->setRecordVideo(static_cast<BElementCheckBox *>(m_gui->getChildByTag(15))->isChecked());
            // CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFrames();
            //  CCTextureCache::sharedTextureCache()->removeAllTextures();
            CCTextureCache::sharedTextureCache()->removeUnusedTextures();
            cocos2d::CCDirector::sharedDirector()->pause();
            
            // m_gui->retain();
            removeChild(m_gui, true);
            // m_gui->release();
            
            CCScene *pScene = BGameScene::scene();
            // run
            CCDirector::sharedDirector()->replaceScene(pScene);
            cocos2d::CCDirector::sharedDirector()->resume();
        };
            break;
            
        default:
            break;
            //CCAssert(0, "Uknow tag");
    };
    
};

void EndGameScene::draw()
{
    
}
#pragma mark - TOUCHES
void EndGameScene::ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    if(!isVisible())
        return;
    CCSetIterator it;
    CCTouch* touch;
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        m_gui->setTouchEnd(location);
    }
};

void EndGameScene::ccTouchesMoved(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    if(!isVisible())
        return;
    CCSetIterator it;
    CCTouch* touch;
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        m_gui->setTouchMove(location);
    }
};

void EndGameScene::ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    //  CCLOG("ccTouchesBegan:: scene %d layer %d ",this->getParent(),this);
    if(!isVisible())
        return;
    CCSetIterator it;
    CCTouch* touch;
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        m_gui->setTouchBegan(location);
    }
};