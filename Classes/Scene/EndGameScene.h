//
//  EndGameScene.h
//  Bra
//
//  Created by PR1.Stigol on 13.03.13.
//
//

#ifndef __Bra__EndGameScene__
#define __Bra__EndGameScene__

#include <iostream>
#include "cocos2d.h"
#include "GuiLayer.h"
#include "MenuScene.h"
using namespace cocos2d;

class EndGameScene : public cocos2d::CCLayer {
public:
    ~EndGameScene();
    EndGameScene();
    static cocos2d::CCScene* scene();
    virtual void draw();
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    void callMenu(CCNode *_sender,void *_d);
    void selectGirl(CCNode *_sender,void *_d);
    void touchGirl(CCNode *_sender,void *_d);
    void animFrame1Cart();
    void animFrame2Cart();
    void animFrame3Cart();
    void animNewGirlCart();
    void createCartoon();
    
    void createReal();
    void animFrame1Real();
    void animFrame2Real();
    void animFrame3Real();
    void animNewGirlReal();
private:
    bool is_start_play;
    GUILayer *m_gui;
    CCSize win_size;
    CCTexture2D *tex;
    
    BGirlPose* m_girl;
    BGirlPose* m_girl_cartoon[3];
    BComponent *anim_girl_cartoon;
    CCNode * node_cart;
    
    BGirlPose* m_girl_r;
    BGirlPose* m_girl_real[3];
    BComponent *anim_girl_real;
    CCNode * node_real;
    CCSprite * border_cartoon;
    CCSprite * border_real;
};
#endif /* defined(__Bra__EndGameScene__) */
