//
//  BraHelper.h
//  Bra
//
//  Created by PR1.Stigol on 29.01.13.
//
//

#ifndef __Bra__BraHelper__
#define __Bra__BraHelper__

#include <iostream>
#include "cocos2d.h"
#include "Scene\BGameScene.h"

const int NUM = 5;


class BraHelper
{
private:
    BraHelper(){ type = -1; record_video = false;count_element = 0;count_component = 0;count_girl_pose = 0;
	
	lock[0] = 5;
	lock[1] = 7; 
	lock[2] = 2; 
	lock[3] = 1; 
	lock[4] = 0; //{ 5, 7, 2, 1, 0 };
	};
   
	 int lock[NUM];// = { 5,7,2,1,0 };
    int type;
    cocos2d::CCPoint camera_position;
    bool record_video;
    int girl_type;
    int score;
    std::list<int> hair_id_list;
public:
    std::list<int> previor_lock_id;
    float camera_scale;
    int count_element;
    int count_component;
    int count_girl_pose;
    int num_girl;
    BGirl* curent_girl;
    bool isHairAcitve(const int &_num)
    {
        std::list<int>::iterator it = hair_id_list.begin();
        while (it != hair_id_list.end()) {
            if(*it == _num)
                return true;
            it++;
        };
        return false;
    };
    void printHairList()
    {
        std::list<int>::iterator it = hair_id_list.begin();
        while (it != hair_id_list.end()) {
            CCLOG("printHairList: %d",*it);
            it++;
        };

    };
    void cleraHairList()
    {
        hair_id_list.clear();
    };
    void addHairToList(const int &_num)
    {
        hair_id_list.push_back(_num);
    };
    
    bool isRecordVideo() const
    {
        return record_video;
    };
    void setRecordVideo(bool _record)
    {
        record_video = _record;
    };
    void setScore(int _score)
    {
        score = _score;
    };
    int getScore()const
    {
        return score;
    };
    
    int getGirlType() const
    {
        return girl_type;
    };
    void setGirlType(const int &_type)
    {
        girl_type = _type;
    };
    const int &getNewType()
    {
        //type = 1 - type;
        type++;
        if(type >= NUM)
            type = 0;
        return lock[type];
    };
    const int& getType() const
    {
        return lock[type];
    };
    
    const cocos2d::CCPoint &unlockCamera() const 
    {
        return camera_position;
    };
    void setCameraPosition(const cocos2d::CCPoint& _pos)
    {
        camera_position = _pos;
    }
    static BraHelper* Instance();
};


#endif /* defined(__Bra__BraHelper__) */
