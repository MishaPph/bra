//
//  BElement.h
//  Bra
//
//  Created by PR1.Stigol on 14.03.13.
//
//

#ifndef __Bra__BElement__
#define __Bra__BElement__

#include "cocos2d.h"
#include <iostream>
#include "BRA.h"


using namespace cocos2d;

class Conteiner
{
    public:
};
class BElementBase
{
protected:
    CCDictionary *m_dict;
    std::vector<std::string> map_file;
    std::list<CCNode *> conteiner_list;
    std::list<CCNode *>::iterator conteiner_it;
    std::string name;
    std::string func_name;
    int value;
    BRA::Etype type;
    bool enabled;
    bool is_frame;
    bool has_container;
    std::string linked;
    bool is_active_touch;
    int name_state;
    CCPoint touch_postion;

    void createContainer(CCArray * _array);
    bool isPointInContainer(const CCPoint& _point);
    bool pointInNode(CCNode* _node,const CCPoint& _point);
public:
    const std::string & getFuncName()
    {
        return func_name;
    };
    int* getValue() {
        return &value;
    };
    std::string & getFileName()
    {
        return map_file[name_state];
    }
    int getStateTexture() const
    {
        return name_state;
    };
    void setName(const char *_name);
    const char* getName();
    virtual bool isPointInObject(const CCPoint &_p) = 0;
    bool isEnabled()
    {
        return enabled;
    };
    void setEnabled(bool _en)
    {
        enabled = _en;
    };
    
    BRA::Etype getType() const
    {
        return type;
    };
};

class BElement : public CCSprite,public BElementBase
{

public:
    BElement();
    virtual ~BElement();

    void setTouchPosition(const CCPoint &_pos)
    {
        touch_postion = _pos;
    };
    
    const std::string* getLinkedElement() const
    {
        return &linked;
    };
    bool isActiveTouch()
    {
        return is_active_touch;
    };
    
    static BElement * create(void);
    bool loadWithDict(CCDictionary *_dict);
    bool setState(const int &_state);
    
    virtual bool moveBy(const CCPoint&){return false;};
    virtual bool moveTo(const CCPoint&){return false;};
    
    virtual void beginTouch(const int = 0) = 0;
    virtual void endTouch(const int = 0) = 0;
    virtual bool initial(CCNode * _parent = NULL) = 0;
    virtual bool isPointInObject(const CCPoint &_p) = 0;
    virtual void reset();
};
#endif /* defined(__Bra__BElement__) */
