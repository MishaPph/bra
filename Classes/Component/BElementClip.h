//
//  BElementClip.h
//  Bra
//
//  Created by PR1.Stigol on 25.03.13.
//
//

#ifndef __Bra__BElementClip__
#define __Bra__BElementClip__

#include <iostream>
#include "BElement.h"
#include "BLinePath.h"

struct MovedClip
{
    BElement* element;
    CCPoint dif;
};

class BElementClip : public BElement
{
private:
    CCPoint diff_point;
    void moveToEnd();
    bool b_return,b_moved,b_lookpoint;
    BLinePath *m_path;
    std::list<MovedClip> move_clip;
    std::list<MovedClip>::iterator mc_it;
    bool is_active_path;
    int m_square;
public:
    bool isActivePath() const
    {
        return is_active_path;
    };
    void setActivePath(bool _b)
    {
        is_active_path = _b;
    };
    void setPathFilter(const unsigned char _filter)
    {
        m_path->setFilter(_filter);
    };
    BElementClip();
    virtual ~BElementClip();
    virtual bool isPointInObject(const CCPoint &_p);
    virtual void beginTouch(const int = 0);
    virtual void endTouch(const int = 0);
    virtual void reset();
    virtual bool moveBy(const CCPoint&);
    virtual bool moveTo(const CCPoint&);
    
    static BElement * create(void);
    void startEnd();
    void moveToStart();
    float getPathProgress() const{
        return m_path->getProgress();
    };
    virtual bool initial(CCNode * _parent = NULL);
};
#endif /* defined(__Bra__BElementClip__) */
