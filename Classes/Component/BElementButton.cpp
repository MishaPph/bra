//
//  BElementButton.cpp
//  Bra
//
//  Created by PR1.Stigol on 14.03.13.
//
//

#include "BElementButton.h"
#include "BComponent.h"

#pragma mark - BUTTON ELEMENT
BElementButton::BElementButton()
{

};

BElementButton::~BElementButton()
{

};

BElement *BElementButton::create(void)
{
    BElement * pRet = new BElementButton();
    pRet->autorelease();
    return pRet;
};

bool BElementButton::isPointInObject(const CCPoint &_p)
{
    return pointInNode(this, _p);
};

bool BElementButton::initial(CCNode * parent)
{
    type = BRA::elBUTTON;
    return true;
};

#pragma mark - BUTTON ELEMENT
BElementButtonNode::BElementButtonNode()
{
    
};

BElementButtonNode::~BElementButtonNode()
{
    
};

BElementButtonNode *BElementButtonNode::create(void)
{
    BElementButtonNode * pRet = new BElementButtonNode();
    pRet->autorelease();
    return pRet;
};

bool BElementButtonNode::isPointInObject(const CCPoint &_p)
{
    CCLOG("BElementButtonNode::isPointInObject");
    CCPoint ang = _p;//ccpRotateByAngle(_p, getPosition(),CC_DEGREES_TO_RADIANS(getRotation()) );
    CCPoint pLB = ccp(getPosition().x - getContentSize().width*getAnchorPoint().x,getPosition().y - getContentSize().height*getAnchorPoint().y);
    CCPoint p = ccp(ang.x - pLB.x,ang.y - pLB.y);
    // if(getRotation() != 0)
    //     p = ccpRotateByAngle(p, ccp(0,0), CC_DEGREES_TO_RADIANS(getRotation()));
    if(p.x > 0 && p.y >0 )
    {
        if((p.x < (getContentSize().width*getScaleX())) && (p.y < (getContentSize().height*getScaleY())))
        {
            if(has_container)
                return isPointInContainer(p);
            else
                return true;
        }
    };
    return false;
};

bool BElementButtonNode::loadWithDict(CCDictionary *_dict)
{
    CCNode::create();
    m_dict = _dict;
    //  CCLOG("loadWithDict::start dict addr %d",m_dict);
    enabled = true;
    name_state = 0;
    initWithFile("node_sprite.png");
    CCPoint size = CCPointFromString(dictStr(m_dict, "Sizes"));
    setContentSize(CCSize( size.x, size.y));
    setScaleX(size.x/getContentSize().width);
    setScaleY(size.y/getContentSize().height);
    if(m_dict->objectForKey("zOrder"))
    {
        _setZOrder(dictInt(m_dict, "zOrder"));
    };
    setTag(dictInt(m_dict, "ID"));

    func_name = dictStr(m_dict, "Event");
    value =  dictInt(m_dict, "Value");
    
    if(m_dict->objectForKey("Enabled"))
        enabled  = dictInt(m_dict,"Enabled") == 1;
    
    setPosition(CCPointFromString(dictStr(m_dict, "Position")));
    setAnchorPoint(CCPointFromString(dictStr(m_dict, "TransformCenter")));
    type = BRA::elBUTTON;
    
    return true;
};

bool BElementButtonNode::initial(CCNode * parent)
{
    type = BRA::elBUTTON;
    return true;
};

