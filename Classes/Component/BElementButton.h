//
//  BElementButton.h
//  Bra
//
//  Created by PR1.Stigol on 14.03.13.
//
//

#ifndef __Bra__BElementButton__
#define __Bra__BElementButton__

#include <iostream>
#include "BElement.h"
#include "BLinePath.h"

class BElementButton : public BElement
{
private:
public:
    BElementButton();
    virtual bool isPointInObject(const CCPoint &_p);
    virtual ~BElementButton();
    virtual void beginTouch(const int = 0){};
    virtual void endTouch(const int = 0){};
    static BElement * create(void);
    virtual bool initial(CCNode * _parent = NULL);
};

class BElementButtonNode : public CCSprite,public BElementBase
{
private:

public:
    bool loadWithDict(CCDictionary *_dict);
    BElementButtonNode();
    virtual ~BElementButtonNode();
    virtual void beginTouch(const int = 0){};
    virtual void endTouch(const int = 0){};
    static BElementButtonNode * create(void);
    virtual bool initial(CCNode * _parent = NULL);
    virtual bool isPointInObject(const CCPoint &_p);
};

#endif /* defined(__Bra__BElementButton__) */
