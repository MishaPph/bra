//
//  BLinePath.h
//  Bra
//
//  Created by PR1.Stigol on 14.03.13.
//
//

#ifndef __Bra__BLinePath__
#define __Bra__BLinePath__

#include <iostream>
#include "cocos2d.h"

struct point{
    signed short x;
    signed short y;
    point(){};
    
    point(signed short _x,signed short _y)
    {
        x = _x; y = _y;
    };
    void set(signed short _x,signed short _y)
    {
        x = _x; y = _y;
    };
    void operator +=(const point &_p)
    {
        x += _p.x;
        y += _p.y;
    }
    void operator -=(const point &_p)
    {
        x -= _p.x;
        y -= _p.y;
    }
    bool operator ==(const point &_p) const 
    {
        return (x == _p.x)&&(y == _p.y);
    }
    };
 enum pathFilter
    {
        fNone    = 0x11111111,
        fSmooth  = 0x00000001,
        fLimit   = 0x00000010
    };
class BLinePath
{
private:
    unsigned char **path_data;
    int prev_telep;
    unsigned char filter;
    bool is_tp_path;
    unsigned short path_height;
    unsigned short path_width;
    point m_point[8];
    std::vector<point> path_list;
    std::vector<point>::iterator path_it,curent_position_it,tmp_it;
    point curent_point,prev_point;
    int path_position;
    int path_length;
    int last_blackposition;
    short last_direct;
    void calcLength(point _start);
    bool getNextPoint(point _start,point _napr);
public:
    BLinePath();
    ~BLinePath();
    float getProgress()const {
        return float(path_position)/float(path_length);
    };
    void init(const char *_filename);
    point& getPosition(){
        return curent_point;
    };
    void setFilter(const unsigned char _filter)
    {
        filter = _filter;
    };
    void goToStart();
    void load(cocos2d::CCDictionary  *_dict);
    bool addMovePoint(const point &_point,bool _rever = false);
    void moveToPoint( short _x, short _y);
    bool isPointInPath(const point &_point);
};
#endif /* defined(__Bra__BLinePath__) */
