//
//  BElement.cpp
//  Bra
//
//  Created by PR1.Stigol on 14.03.13.
//
//

#include "BElement.h"
#include "BraHelper.h"

#pragma mark - BASE ELEMENT
BElement::BElement()
{
    type = BRA::elNONE;
    enabled = true;
    has_container = false;
    is_frame = false;
    is_active_touch = false;
    BraHelper::Instance()->count_element++;
    CCLOG("BElement::BElement() count BElement %d",BraHelper::Instance()->count_element);
};

BElement::~BElement()
{
    BraHelper::Instance()->count_element--;
    CCLOG("BElement::~BElement() count BElement %d",BraHelper::Instance()->count_element);
    if(has_container)
    {
        conteiner_it = conteiner_list.begin();
        while (conteiner_it != conteiner_list.end()) {
           // CC_SAFE_RELEASE_NULL((*conteiner_it));
            conteiner_it++;
        }
    }
}

BElement * BElement::create(void)
{
    /*  BElement * pRet = new BElement();
     pRet->autorelease();
     return pRet;*/
    return NULL;
};

bool BElement::loadWithDict(CCDictionary *_dict)
{
    m_dict = _dict;
    //  CCLOG("loadWithDict::start dict addr %d",m_dict);
    
    if(m_dict->objectForKey("ClipNames"))
    {
        CCArray* frameArray = (CCArray*)m_dict->objectForKey("ClipNames");
        CCObject* pObj = NULL;
        CCARRAY_FOREACH(frameArray, pObj)
        {
           // CCLOG("load image %s",static_cast<CCString*>(pObj)->getCString());
            map_file.push_back(static_cast<CCString*>(pObj)->getCString());
        };
    };
    name_state = 0;

    if(strcmp(dictStr(m_dict, "Type"),"ButtonNode") == 0)
    {
        CCPoint size = CCPointFromString(dictStr(m_dict, "Sizes"));
        setContentSize(CCSize(size.x, size.y));
    }
    else
    {
        if(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(map_file[0].c_str()))
        {
            is_frame = true;
            initWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(map_file[0].c_str()));
        }
        else
        {
            is_frame = false;
            initWithFile(map_file[0].c_str());
        };
    }
    if(m_dict->objectForKey("zOrder"))
    {
        _setZOrder(dictInt(m_dict, "zOrder"));
    };
    setTag(dictInt(m_dict, "ID"));
    
    //linked = NULL;
    linked.clear();
    if(m_dict->objectForKey("Linked"))
    {
        linked = dictStr(m_dict, "Linked");
    };
    if(m_dict->objectForKey("Rotation"))
    {
        setRotation(dictInt(m_dict, "Rotation"));
    };
    func_name = dictStr(m_dict, "Event");
    value =  dictInt(m_dict, "Value");
    
    if(m_dict->objectForKey("Enabled"))
        enabled  = dictInt(m_dict,"Enabled");
    
    setPosition(CCPointFromString(dictStr(m_dict, "Position")));
    setAnchorPoint(CCPointFromString(dictStr(m_dict, "TransformCenter")));

    if(m_dict->objectForKey("Container"))
        createContainer(dictArray(m_dict, "Container"));

    
    if(m_dict->objectForKey("Animation"))
    {
        CCDictionary *an_dict = dictDict(m_dict, "Animation");
        if(an_dict->objectForKey("Frame"))
        {
            CCAnimation* anima = CCAnimation::create();
           // char str[NAME_MAX];

            std::vector<std::string>::iterator it = map_file.begin();
            while (it != map_file.end()) {
                anima->addSpriteFrameWithFileName((*it).c_str());
                it++;
            };

            anima->setDelayPerUnit(1.0);
            anima->retain();
            
            CCAnimate* animate = CCAnimate::create( anima );
            runAction(CCRepeatForever::create( animate ));
        };
    }
    return true;
};

void BElementBase::createContainer(CCArray * _array)
{
    CCObject * obj = NULL;
    has_container = true;
    CCARRAY_FOREACH(_array, obj)
    {
        CCDictionary * dict = static_cast<CCDictionary*>(obj);
        CCNode* node = CCNode::create();
       // CCSprite *node = CCSprite::create("RButtonLock6.png");
        node->setPosition(CCPointFromString(dictStr(dict, "Position")));
        CCPoint size = CCPointFromString(dictStr(dict, "Sizes"));
        node->setContentSize(CCSize(size.x, size.y));
        node->setAnchorPoint(ccp(0.5,0.5));
        CCLOG("createContainer setContentSize:: %f %f",node->getContentSize().width,node->getContentSize().height);
        conteiner_list.push_back(node);
        static_cast<BElement *>(this)->addChild(node);
    };
};

void BElement::reset()
{
    CCLOG("BElement::reset");
    setPosition(CCPointFromString(dictStr(m_dict, "Position")));
    enabled = true;
    setVisible(true);
    if(m_dict->objectForKey("Enabled"))
        enabled  = dictInt(m_dict,"Enabled");
    if(m_dict->objectForKey("Visible"))
          setVisible(dictInt(m_dict,"Visible") != 0);
    
    setRotation(0);
    if(m_dict->objectForKey("Rotation"))
    {
        setRotation(dictInt(m_dict, "Rotation"));
    };
    setState(0);
    stopAllActions();
};

bool BElement::setState(const int &_state)
{
    
    if(_state < map_file.size() && _state >= 0)
    {
        
       // CCLOG("BElement::setState %d name %s",_state,map_file[_state].c_str());
        if(is_frame)
        {
            CCSpriteFrame * sframe = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(map_file[_state].c_str());
            if(sframe)
            {
                setTextureRect(sframe->getRect(),sframe->isRotated(),sframe->getOriginalSize());
                name_state  = _state;
            }
        }
        else
        {
            CCLOG("BElement::setState %s",map_file[_state].c_str());
            name_state  = _state;
            setTexture(CCTextureCache::sharedTextureCache()->addImage(map_file[_state].c_str()));
        };
        
        return true;
    };
    return false;
};

const char* BElementBase::getName()
{
    return name.c_str();
};

void BElementBase::setName(const char *_name)
{
    name = _name;
};

bool BElementBase::isPointInContainer(const CCPoint& _point)
{
   // bool p = false;//collide
    conteiner_it = conteiner_list.begin();
    while (conteiner_it != conteiner_list.end()) {
    
    CCNode *node = *conteiner_it;
   // CCPoint p = getChildByTag(1)->convertToNodeSpace(_point);
    
    CCPoint pLB = ccp(node->getPosition().x - node->getContentSize().width*node->getAnchorPoint().x,node->getPosition().y - node->getContentSize().height*node->getAnchorPoint().y);
    CCPoint p = ccp(_point.x - pLB.x,_point.y - pLB.y);
    if(p.x > 0 && p.y >0 )
    {
        if((p.x < (node->getContentSize().width*node->getScaleX())) && (p.y < (node->getContentSize().height*node->getScaleY())))
        {
         //   CCLOG("isPointInContainer:: %f %f",_point.x,_point.y);
            return true;
        }
    };
      //  node->setPosition(_point);
        conteiner_it++;
    }
    
    return false;
};
bool BElementBase::pointInNode(CCNode* _node,const CCPoint &_p)
{
    CCPoint ang = _p;//ccpRotateByAngle(_p, getPosition(),CC_DEGREES_TO_RADIANS(getRotation()) );
    CCPoint pLB = ccp(_node->getPosition().x - _node->getContentSize().width*_node->getAnchorPoint().x,_node->getPosition().y - _node->getContentSize().height*_node->getAnchorPoint().y);
    CCPoint p = ccp(ang.x - pLB.x,ang.y - pLB.y);
    if(p.x > 0 && p.y >0 )
    {
        if((p.x < (_node->getContentSize().width*_node->getScaleX())) && (p.y < (_node->getContentSize().height*_node->getScaleY())))
        {
            if(has_container)
                return isPointInContainer(p);
                else
                    return true;
        }
    };
    return false;
};

