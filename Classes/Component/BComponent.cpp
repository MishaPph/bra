//
//  BComponent.cpp
//  Bra
//
//  Created by PR1.Stigol on 01.03.13.
//
//

#include "BComponent.h"
#include "BRA.h"
#include "BElementButton.h"
#include "BraHelper.h"
#include "BElementAngleClip.h"
#include "BElementClip.h"

#pragma mark - CLIPPER ELEMENT
BElementClipper::BElementClipper()
{
    
};

BElementClipper::~BElementClipper()
{
    
};

BElement * BElementClipper::create(void)
{
    BElement * pRet = new BElementClipper();
    pRet->autorelease();
    return pRet;
};

bool BElementClipper::initial(CCNode * parent)
{
    type = BRA::elCLIPPER;
    return true;
};
bool BElementClipper::isPointInObject(const CCPoint &_p)
{
    return pointInNode(this, _p);
};
#pragma mark - PICTURE ELEMENT
BElementPicture::BElementPicture()
{
    
};

BElementPicture::~BElementPicture()
{
    
};

BElement * BElementPicture::create(void)
{
    BElement * pRet = new BElementPicture();
    pRet->autorelease();
    return pRet;
};

bool BElementPicture::isPointInObject(const CCPoint &_p)
{
    return pointInNode(this, _p);
};

bool BElementPicture::initial(CCNode * parent)
{
    type = BRA::elPICTURE;
    return true;
};
#pragma mark - RADIOBUTTON ELEMENT

BElementRadioButton::~BElementRadioButton()
{
    
};
bool BElementRadioButton::isPointInObject(const CCPoint &_p)
{
    return pointInNode(this, _p);
};
BElement * BElementRadioButton::create(void)
{
    BElement * pRet = new BElementRadioButton();
    pRet->autorelease();
    return pRet;
};
bool BElementRadioButton::initial(CCNode * parent)
{
    type = BRA::elRADIOBUTTON;
    return true;
};
#pragma mark - LABEL ELEMENT
BElementLabel::BElementLabel()
{
    
}
void BElementLabel::setString(const std::string &_str)
{
    
}
void BElementLabel::setString(const int &_value)
{
    
};
BElementLabel::~BElementLabel()
{
    
};

BElementLabel * BElementLabel::create(void)
{
    BElementLabel * pRet = new BElementLabel();
    pRet->autorelease();
    return pRet;
};

bool BElementLabel::initial(CCNode * parent)
{
	return true;
};


bool BElementLabel::loadWithDict(CCDictionary *_dict)
{
//    m_dict = _dict;
    //  CCLOG("loadWithDict::start dict addr %d",m_dict);
    const char* font_name;
    if(_dict->objectForKey("ClipNames"))
    {
        CCArray* frameArray = (CCArray*)_dict->objectForKey("ClipNames");
        CCObject* pObj = NULL;
        CCARRAY_FOREACH(frameArray, pObj)
        {
            //CCLOG("load image %s",static_cast<CCString*>(pObj)->getCString());
            font_name = static_cast<CCString*>(pObj)->getCString();
           // map_file.push_back(static_cast<CCString*>(pObj)->getCString());
        };
    };
    //cocos2d::CCLabelTTF::create("asdads", "Arial", 24);
    CCDictionary *spec = dictDict(_dict, "Special");
    initWithString(dictStr(spec, "Text"), font_name, dictFloat(spec, "TextSize"));
   // dictFloat(_dict, "TextSize")
    if(_dict->objectForKey("zOrder"))
    {
        _setZOrder(dictInt(_dict, "zOrder"));
    };
    setTag(dictInt(_dict, "ID"));
    setPosition(CCPointFromString(dictStr(_dict, "Position")));
    setAnchorPoint(CCPointFromString(dictStr(_dict, "TransformCenter")));
    return true;
};

#pragma mark - CHECKBOX ELEMENT
BElementCheckBox::BElementCheckBox()
{
    is_checked = false;
};

BElementCheckBox::~BElementCheckBox()
{
    
};

void BElementCheckBox::setChecked(bool _check)
{
    is_checked = _check;
    if(is_checked)
    {
        setState(0);
    }
    else
    {
        setState(1);
    };
};

void BElementCheckBox::beginTouch(const int d)
{
   // setState(1);
};

void BElementCheckBox::endTouch(const int d)
{
   // setState(0);
};

BElement * BElementCheckBox::create(void)
{
    BElement * pRet = new BElementCheckBox();
    pRet->autorelease();
    return pRet;
};
bool BElementCheckBox::isPointInObject(const CCPoint &_p)
{
    return pointInNode(this, _p);
};
bool BElementCheckBox::initial(CCNode * parent)
{
    type = BRA::elCHECKBOX;
    return true;
};

#pragma mark - BCOMPONENT
BComponent::BComponent()
{
    m_dict = NULL;
    is_load = false;
    BraHelper::Instance()->count_component++;
    CCLOG("BComponent::BComponent() count %d",BraHelper::Instance()->count_component);
};
std::list<std::string> BComponent::list_name;

BComponent::~BComponent()
{
   // CCLOG("destroy ~BComponent %d",this);
    //unloadAtlasTexture();
  //  m_dict->retain();
   // CCNode::removeAllChildrenWithCleanup(true);
    BraHelper::Instance()->count_component--;
    CCLOG("BComponent::~BComponent() count %d FileName %s",BraHelper::Instance()->count_component,file_name.c_str());
    
    BComponent::list_name.remove(file_name);
    it_name = BComponent::list_name.begin();
    while (it_name != BComponent::list_name.end()) {
        CCLOG("FileName %s",(*it_name).c_str());
        it_name++;
    }
    if(m_dict->retainCount() > 0)
    CC_SAFE_RELEASE_NULL(m_dict);
};

BComponent * BComponent::create()
{
    BComponent * pRet = new BComponent();
	pRet->autorelease();
    CCLOG("BComponent::create %d",pRet);
	return pRet;
};

BComponent * BComponent::createBath()
{
    
    BComponent * pRet = new BComponent();
	pRet->autorelease();
    CCLOG("BComponent::create %d",pRet);
	return pRet;
};

void BComponent::draw(void)
{
    
};

BElement *BComponent::getTouchElement(const CCPoint &_touch,int _filter)
{
    CCLOG("getTouchElement::getChildrenCount %d",getChildrenCount());
    std::list<BElement*> tmp_element;
    CCArray* array = getChildren();
    CCObject * tmp = NULL;
    CCARRAY_FOREACH(array, tmp)
    {
        BElement* elem = static_cast<BElement*>(tmp);

        if(elem && elem->isVisible() && elem->isEnabled())
        {
            BElementBase * base = elem;
             if(base->isPointInObject(convertToNodeSpace(_touch)))
             {
                if(base->getType() & _filter)
                {
                    CCLOG("push %s",elem->getName());
                    tmp_element.push_back(elem);
                }
                if(_filter & BRA::elNONE) // if not filtered then return first find touch element
                {
                    return elem;
                }
             }
            
        };
    };

    if(!tmp_element.empty())
    {
       // bool find max zorder obejct
        it = tmp_element.begin();
        int max_zorder = 0;
        while (it != tmp_element.end()) {
            {
                CCLOG("(*it)->getZOrder() %d",(*it)->getZOrder());
            if((*it)->getZOrder() > max_zorder)
                max_zorder = (*it)->getZOrder();
            }
            it++;
        };
        
        //return element with max zOrder
        it = tmp_element.begin();
        while (it != tmp_element.end()) {
            if(max_zorder == (*it)->getZOrder())
            {
                if(!(*it)->getLinkedElement()->empty()) //if have linked element then return linked element
                {
                    return getChildByName((*it)->getLinkedElement()->c_str());
                }
                else
                {
                    return *it;
                };
            };
            it++;
        }
        return *(tmp_element.begin());
    }
    return NULL;
};

BElement* BComponent::createElementWithName(const char *_type)
{
    if(strcmp(_type, "Button") == 0)
    {
        return BElementButton::create();
    }
    else if(strcmp(_type, "Clipper") == 0)
    {
        return BElementClipper::create();
    }
    else if(strcmp(_type, "Picture") == 0)
    {
        return BElementPicture::create();
    }
    else if(strcmp(_type, "CheckBox") == 0)
    {
        return BElementCheckBox::create();
    }else if(strcmp(_type, "RadioButton") == 0)
    {
        return BElementRadioButton::create();
    }
    else if(strcmp(_type, "Clip") == 0)
    {
        return BElementClip::create();
    }
    else if(strcmp(_type, "AngleClip") == 0)
    {
        return BElementAngleClip::create();
    }
    return NULL;
};

BElement* BComponent::getChildByName(const char *_name)
{
    if(strlen(_name) < 4)
    {
        return static_cast<BElement*>(getChildByTag(atoi(_name)));
    };
    CCArray *m_array = getChildren();
    CCObject *mObj = NULL;
    CCARRAY_FOREACH(m_array,mObj)
    {
        if(strcmp(_name,static_cast<BElement*>(mObj)->getName()) == 0)
        {
            return static_cast<BElement*>(mObj);
        }
    };
    return NULL;
};

void BComponent::runComplite()
{

    CCDictionary* compliteDic = (CCDictionary*)m_dict->objectForKey("complite");
    CCArray *m_array = (CCArray *)compliteDic->objectForKey("reparentChild");
    CCObject *mObj = NULL;
    CCARRAY_FOREACH(m_array,mObj)
    {
        CCDictionary * tdict = (CCDictionary*)mObj;
        CCNode* base = getChildByName(dictStr(tdict, "parent"));
        CCArray *t_array = (CCArray *)tdict->objectForKey("child");
        CCObject *t_obj = NULL;
        CCARRAY_FOREACH(t_array,t_obj)
        {
            CCNode *node_b = getChildByName(static_cast<CCString *>(t_obj)->getCString());
            if(!node_b)
                continue;
            node_b->retain();
            node_b->setScale(1.0);
            CCSize win_size = CCDirector::sharedDirector()->getWinSize();
            CCPoint poin_base = ccp(base->getPosition().x/1024.0,base->getPosition().y/768.0);
            CCPoint poin_node = ccp(node_b->getPosition().x/1024.0,node_b->getPosition().y/768.0);
            CCPoint pos = ccpSub(node_b->getPosition(),base->getPosition());
            node_b->removeFromParentAndCleanup(false);
            node_b->autorelease();
            if(win_size.width == 1024)
            {
                node_b->setPositionX(pos.x/1024.0*win_size.width + base->getContentSize().width*0.5);
                node_b->setPositionY(pos.y/768.0*win_size.height + base->getContentSize().height*0.5);
            }
            else
            {
                if(win_size.width == 568)
                    node_b->setPositionX(pos.x/1024.0*win_size.width*0.97 + base->getContentSize().width*0.5);
                else
                    node_b->setPositionX(pos.x/1024.0*win_size.width*1.05 + base->getContentSize().width*0.5);
                node_b->setPositionY(pos.y/768.0*win_size.height*1.15 + base->getContentSize().height*0.5);
            }
            base->addChild(node_b);
        };
    };
};

void BComponent::reset()
{
//    //Need find to error uknow elemen in this component
//    CCLOG("getTouchElement::reset %d",getChildrenCount());
//    CCArray* array = getChildren();
//    CCObject * tmp = NULL;
//    CCARRAY_FOREACH(array, tmp)
//    {
//        BElement* elem = static_cast<BElement*>(tmp);
//        CCLOG("getTouchElement::elem %d %d",elem,elem->getType());
//        elem->reset();
//        elem->retain();
//        elem->removeFromParentAndCleanup(true);
//        this->addChild(elem,elem->getZOrder());
//        elem->autorelease();
//    };
    removeAllChildrenWithCleanup(true);
    CCDictionary* frameDic = (CCDictionary*)m_dict->objectForKey("frames");
    if(!frameDic)
    {
        return;
    };
    CCDictElement* pElement = NULL;
    CCDICT_FOREACH(frameDic, pElement)
    {
      //  CCLOG("BComponent::loadWithFile:: dict key %s",pElement->getStrKey());
        CCDictionary * _tdict = (CCDictionary*)pElement->getObject();
        BElement* elem = createElementWithName(dictStr(_tdict,"Type"));
        if(elem == NULL)
        {
            CCLOG("Error::incorect Type  of BElement %s",dictStr(_tdict,"Type"));
            if(strcmp(dictStr(_tdict,"Type"), "Label") == 0)
            {
                BElementLabel * label = BElementLabel::create();
                //CCLabelTTF *label = cocos2d::CCLabelTTF::create("asdads", "Arial", 24);
                label->loadWithDict((CCDictionary*)pElement->getObject());
                this->addChild(label,label->getZOrder());
            };
            
        }
        else
        {
            elem->loadWithDict((CCDictionary*)pElement->getObject());
            elem->initial(this);
            CCLOG("elem getText %d",elem->getTexture()->retainCount());
            this->addChild(elem,elem->getZOrder());
            // CCLOG("%d %d",elem);
            elem->setName(pElement->getStrKey());
            //m_element.push_back(elem);
        }
    }
    is_load = true;
};
void BComponent::unloadAtlasTexture()
{
 /*   std::vector<CCTexture2D *>::iterator _it2 = atlas_texture.begin();
    while (_it2 != atlas_texture.end()) {
        //CCLOG("removeAtlasPlist::remove texture %s",(*_it).c_str());
        //CCTextureCache::sharedTextureCache()->removeTextureForKey((*_it).c_str());
        //  if(*_it2 != NULL)
        
        //while ((*_it2) != NULL && (*_it2)->retainCount() != 0) {
        CCLOG("unloadAtlasTexture::count %d",(*_it2)->retainCount());
       // CC_SAFE_RELEASE_NULL(*_it2);
        //  }
        _it2++;
    };*/
    
    std::vector<std::string>::iterator _it = atlas_map.begin();
    while (_it != atlas_map.end()) {
        CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile((*_it).c_str());
        _it++;
    };
    
    
    _it = atlas_texture_name.begin();
  //  while (_it != atlas_texture_name.end()) {
   //     CCLOG("removeAtlasPlist::remove texture %s",(*_it).c_str());
      //  CCTextureCache::sharedTextureCache()->removeTextureForKey((*_it).c_str());
      //  _it++;
  //  };
    std::vector<CCTexture2D *>::iterator _it2 = atlas_texture.begin();
    while (_it2 != atlas_texture.end()) {
        //CCLOG("removeAtlasPlist::remove texture %s",(*_it).c_str());
        //CCTextureCache::sharedTextureCache()->removeTextureForKey((*_it).c_str());
      //  if(*_it2 != NULL)
        
        //while ((*_it2) != NULL && (*_it2)->retainCount() != 0) {
            CCLOG("unloadAtlasTexture::count %d name %s",(*_it2)->retainCount(),(*_it).c_str());
        if((*_it2)->retainCount() > 0)
            CC_SAFE_RELEASE_NULL(*_it2);
      //  }
        _it++;
        _it2++;
    };
    atlas_texture.clear();
};

void BComponent::loadAtlasTexture()
{
    std::vector<std::string>::iterator _it = atlas_map.begin();
    std::vector<std::string>::iterator _it2 = atlas_texture_name.begin();
    while (_it != atlas_map.end()) {
        CCTexture2D* text = CCTextureCache::sharedTextureCache()->addImage((*_it2).c_str());
        CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile((*_it).c_str(),text);
        atlas_texture.push_back(text);
        _it2++;
        _it++;
    };
};

bool BComponent::loadWithFile(const char* _filename)
{
    m_dict = CCDictionary::createWithContentsOfFile(_filename);
    CCLog("BComponent:: loadWithFile %s",_filename);
    file_name = _filename;
    BComponent::list_name.push_back(_filename);
    if(!m_dict)
    {
        CCLog("BComponent::Error:: incorect file name");
        return false;
    };
    CCDictionary* frameDic = (CCDictionary*)m_dict->objectForKey("frames");
    if(!frameDic)
    {
        return false;
    };
    CCDictionary* metaDic = (CCDictionary*)m_dict->objectForKey("metadata");
    CCArray * m_array = dictArray(metaDic, "atlasFileName");
    CCObject * pObj;
    atlas_map.clear();
    atlas_texture.clear();
    CCARRAY_FOREACH(m_array, pObj)
    {
            const char* frameName = ((CCString*)pObj)->getCString();
           // CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(frameName);
            atlas_map.push_back(frameName);
            
            //save texture file name for delete
            const char *pszPath = CCFileUtils::sharedFileUtils()->fullPathFromRelativeFile(frameName, frameName);
            CCDictionary *tdict = CCDictionary::createWithContentsOfFileThreadSafe(frameName);
            
            std::string texturePath("");
            
            CCDictionary* metadataDict = (CCDictionary*)tdict->objectForKey("metadata");
            if (metadataDict)
            {
                // try to read  texture file name from meta data
                texturePath = metadataDict->valueForKey("textureFileName")->getCString();
            }
            
            if (! texturePath.empty())
            {
                // build texture path relative to plist file
                texturePath = CCFileUtils::sharedFileUtils()->fullPathFromRelativeFile(texturePath.c_str(), pszPath);
            }
            if (! texturePath.empty())
                atlas_texture_name.push_back(texturePath);
        CCLOG("texturePath.c_str() %s",texturePath.c_str());
            CCTexture2D* text = CCTextureCache::sharedTextureCache()->addImage(texturePath.c_str());
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(frameName,text);
            atlas_texture.push_back(text);
            tdict->release();
        
    };
    
    glob_type = 1;
    if(metaDic->objectForKey("Type"))
    {
        glob_type = dictInt(metaDic, "Type");
    };
    
    m_dict->retain();
    CCDictElement* pElement = NULL;
    CCDICT_FOREACH(frameDic, pElement)
    {
      //  CCLOG("BComponent::loadWithFile:: dict key %s",pElement->getStrKey());
        CCDictionary * _tdict = (CCDictionary*)pElement->getObject();
        BElement* elem = createElementWithName(dictStr(_tdict,"Type"));
        if(elem == NULL)
        {
            CCLOG("Error::incorect Type  of BElement %s",dictStr(_tdict,"Type"));
            if(strcmp(dictStr(_tdict,"Type"), "Label") == 0)
            {
                BElementLabel * label = BElementLabel::create();
                //CCLabelTTF *label = cocos2d::CCLabelTTF::create("asdads", "Arial", 24);
                label->loadWithDict((CCDictionary*)pElement->getObject());
                this->addChild(label,label->getZOrder());
            }else if(strcmp(dictStr(_tdict,"Type"), "ButtonNode") == 0)
            {
                BElementButtonNode* node = BElementButtonNode::create();
              //  node->retain();
                node->loadWithDict((CCDictionary*)pElement->getObject());
                this->addChild(node,node->getZOrder());
                node->setName(pElement->getStrKey());
                node->initial(this);
            }
                
        }
        else
        {
            elem->loadWithDict((CCDictionary*)pElement->getObject());
            elem->initial(this);
            //CCLOG("elem getText %d",elem->getTexture()->retainCount());
            this->addChild(elem,elem->getZOrder());
           // CCLOG("%d %d",elem);
            elem->setName(pElement->getStrKey());
            //m_element.push_back(elem);
        }
    }
    is_load = true;
    return true;
};
