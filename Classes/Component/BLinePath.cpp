//
//  BLinePath.cpp
//  Bra
//
//  Created by PR1.Stigol on 14.03.13.
//
//

#include "BLinePath.h"
#include "BRA.h"
using namespace cocos2d;

#pragma mark - PATH LINE
BLinePath::BLinePath()
{
    prev_telep = 0;
    is_tp_path = 0;
    curent_point.set(0, 0);
    m_point[0].set(-1, -1);
    m_point[1].set(0, -1);
    m_point[5].set(0, 1);
    
    m_point[2].set(1, -1);
    m_point[4].set(1, 1);
    m_point[6].set(-1, 1);
    
    m_point[7].set(-1, 0);
    m_point[3].set(1, 0);
};

BLinePath::~BLinePath()
{
    for(int i = 0; i < path_width; ++i)
    {
        delete[] path_data[i];
        path_data[i] = NULL;
    };
    delete[] path_data;
    path_data = NULL;
};

bool BLinePath::isPointInPath(const point &_point)
{
    if(_point.y < 0 || _point.y >= path_height)
        return false;
    
    if(_point.x < 0 || _point.x >= path_width)
        return false;
    
    if(path_data[_point.x][_point.y] == 0)
        return false;
    
    return true;
};
void BLinePath::calcLength(point _start)
{
    //  CCLOG("calcPathLength %d %d",_start.x,_start.y);
    if(!isPointInPath(_start))
    {
        CCLOG("isPoint not on path %d %d %d",_start.x,_start.y,path_data[_start.x][_start.y]);
        return;
    }
    if(path_data[_start.x][_start.y] == 0 || path_data[_start.x][_start.y] == 255)
    {
        CCLOG("Error::calcPathLength:: %d",path_data[_start.x][_start.y]);
        return;
    }
    path_data[_start.x][_start.y]++;
    
    point p = _start;
    if(!path_list.empty())
    {
        point tp(p.x - prev_point.x,p.y - prev_point.y);
        //   CCLOG("calcPathLength:: atan2 %f",atan2(p.x - prev_point.x, p.y - prev_point.y));
        path_list.push_back(tp);
        cocos2d::CCPoint poi = ccp(p.x - prev_point.x, p.y - prev_point.y);
        ccpNormalize(poi);
    }
    else
    {
        //   find(path_list.begin(), path_list.end(), p);
        path_list.push_back(p);
    };
    
    prev_point = p;
    path_length++;
    // CCLOG("start loop");
    for (int i = 0; i < 8; i++) {
        p = _start;
        p += m_point[i];
        //  CCLOG("loop calcPathLength::%d %d",p.x,p.y);
        
        if(isPointInPath(p))
        {
            
            if(path_data[p.x][p.y] < 255 && path_data[p.x][p.y] > 0)
            {
                calcLength(p);
            }
        }
        else
        {
            //   CCLOG("!calcPathLength:: %d",path_data[p.x][p.y]);
        }
    };
    //CCLOG("Error::calcPathLength:: %d %d %d",path_data[_start.x][_start.y],_start.x,_start.y);
};
void BLinePath::moveToPoint( short _x, short _y)
{
    point napr;
    napr.x = (_x - curent_point.x);
    napr.y = (_y - curent_point.y);
    
    const int DEV = 3;
    if(abs(napr.x) < DEV)
        napr.x = 0;
    if(abs(napr.y) < DEV)
        napr.y = 0;
    // int angle = int(atan2(napr.x, napr.y)*PI180);
    if(napr.x != 0)
        napr.x /= abs(napr.x);
    if(napr.y != 0)
        napr.y /= abs(napr.y);
    
    if(napr.x == 0 && napr.y == 0)
    {
        return;
    };
    
    int start_prg = path_position;
   // int start_color = path_data[curent_point.x][curent_point.y];
    point prev_point = curent_point;
    
    // CCLOG("x %d y %d need(%d,%d) pos %d %d %d",_x,_y,curent_point.x,curent_point.y,path_position,path_length,path_data[curent_point.x][curent_point.y]);
    bool b = false;
    if(path_data[curent_point.x][curent_point.y] != 1)
    {
        while (getNextPoint(curent_point,napr)) {
            if(prev_point.x == curent_point.x && prev_point.y == curent_point.y)
                break;
            if(path_data[curent_point.x][curent_point.y] == 1)
                break;
            // CCLOG("x %d y %d need(%d,%d) pos %d %d %d",_x,_y,curent_point.x,curent_point.y,path_position,path_length,path_data[curent_point.x][curent_point.y]);
            
            napr.x = (_x - curent_point.x);
            napr.y = (_y - curent_point.y);
            if(abs(napr.x) < DEV)
                napr.x = 0;
            if(abs(napr.y) < DEV)
                napr.y = 0;
            if(napr.x == 0 && napr.y == 0)
            {
                break;
            };
            if(napr.x != 0)
                napr.x /= abs(napr.x);
            if(napr.y != 0)
                napr.y /= abs(napr.y);
            if(b) //check tremor point
                prev_point = curent_point;
            b = !b;
            // CCLOG("x %d y %d need(%d,%d) pos (%d %d) %d",_x,_y,curent_point.x,curent_point.y,napr.x,napr.y,path_data[curent_point.x][curent_point.y]);
        };
    };
    //return;
    int end_prg = path_position;
    if(abs(end_prg - start_prg) > 1)
    {
        last_blackposition = start_prg;
    }
    if(path_data[curent_point.x][curent_point.y] == 1 )
    {
        is_tp_path = true;
        //  CCLOG("moveToPoint color %d %d %d %d",path_data[curent_point.x][curent_point.y],curent_point.x,last_blackposition,end_prg - start_prg);
        if((end_prg - last_blackposition) > 0)
        {
            do {
                // CCLOG("moveToPoint color %d %d",path_data[curent_point.x][curent_point.y],path_position);
                curent_position_it++;
                curent_point += *curent_position_it;
                path_position++;
            } while (path_data[curent_point.x][curent_point.y] == 1);
            //curent_point -= *curent_position_it;
            //curent_position_it--;
            //path_position--;
            prev_telep++;
            //   CCLOG("end teleport color %d %d",path_data[curent_point.x][curent_point.y],path_position);
        } else if((end_prg - last_blackposition) < 0)
        {
            do {
                curent_point -= *curent_position_it;
                path_position--;
                curent_position_it--;
            } while (path_data[curent_point.x][curent_point.y] == 1);
            
        };
    }else
    {
        //last_blackposition = path_position;
    }
   // CCLOG("moveToPoint color %d %d %d",path_data[curent_point.x][curent_point.y],curent_point.x,curent_point.y);
    // curent_point;
};
bool BLinePath::addMovePoint(const point &_point,bool _rever)
{
    //CCLOG("addMovePoint %d %d",_point.x,_point.y);
    if(isPointInPath(point(curent_point.x + _point.x,curent_point.y + _point.y)))
    {
        curent_point += _point;
        if(!(path_data[curent_point.x][curent_point.y] & filter))
        {
            curent_point -=_point;
            return false;
        };
        if(_rever)
        {
            tmp_it--;
            path_position--;
            last_direct = -1;
        }
        else
        {
            path_position++;
            last_direct = 1;
            
        };
        curent_position_it = tmp_it;
        return true;
    };
    return false;
};
bool BLinePath::getNextPoint(point _start,point _napr)
{
    // point tmp = _start;
    if(_start.y < 0 || _start.y > path_height)
    {
        CCLOG("Error a");
        return false;
    }
    if(_start.x < 0 || _start.x > path_width)
    {
        CCLOG("Error b");
        return false;
    }
    if(path_data[_start.x][_start.y] == 0)
    {
        // CCLOG("Error c");
        return false;
    }
    int p = -1;
    for (int  i = 0; i < 8; ++i) {
        if(m_point[i].x == _napr.x && m_point[i].y == _napr.y)
        {
            p = i;
            break;
        };
    };
    // CCLOG("getNextPoint %d",rand());
    if( p != -1)
    {
        int p_prev = p - 1;
        if(p_prev < 0)
        {
            p_prev = 7;
        };
        int p_prev2 = p_prev - 1;
        if(p_prev2 < 0)
        {
            p_prev2 = 7;
        }
        int p_next = p + 1;
        if(p_next > 7)
        {
            p_next = 0;
        };
        
        int p_next2 = p_next + 1;
        if(p_next2 > 7)
        {
            p_next2 = 0;
        };
        //  CCLOG("napr %d (%d %d) (%d %d)", p,p_prev,p_prev2,p_next,p_next2);
        tmp_it = curent_position_it;
        if(tmp_it != path_list.end())
        {
            tmp_it++;
            
            if((*tmp_it).x == m_point[p].x && (*tmp_it).y == m_point[p].y )
            {
                if(addMovePoint(m_point[p]))
                    return true;
            };
            if((*tmp_it).x == m_point[p_next].x && (*tmp_it).y == m_point[p_next].y)
            {
                if(addMovePoint(m_point[p_next]))
                    return true;
            };
            if((*tmp_it).x == m_point[p_prev].x && (*tmp_it).y == m_point[p_prev].y )
            {
                if(addMovePoint(m_point[p_prev]))
                    return true;
            };
            if((*tmp_it).x == m_point[p_next2].x && (*tmp_it).y == m_point[p_next2].y)
            {
                if(addMovePoint(m_point[p_next2]))
                    return true;
            };
            if((*tmp_it).x == m_point[p_prev2].x && (*tmp_it).y == m_point[p_prev2].y )
            {
                if(addMovePoint(m_point[p_prev2]))
                    return true;
            };
        };
        tmp_it = curent_position_it;
        
        if(tmp_it != path_list.begin())
        {
            if((*tmp_it).x == -m_point[p].x && (*tmp_it).y == -m_point[p].y )
            {
                if(addMovePoint(m_point[p],true))
                    return true;
            };
            if((*tmp_it).x == -m_point[p_next].x && (*tmp_it).y == -m_point[p_next].y )
            {
                if(addMovePoint(m_point[p_next],true))
                    return true;
            };
            if((*tmp_it).x == -m_point[p_prev].x && (*tmp_it).y == -m_point[p_prev].y)
            {
                if(addMovePoint(m_point[p_prev],true))
                    return true;
            };
            if((*tmp_it).x == -m_point[p_next2].x && (*tmp_it).y == -m_point[p_next2].y )
            {
                if(addMovePoint(m_point[p_next2],true))
                    return true;
            };
            if((*tmp_it).x == -m_point[p_prev2].x && (*tmp_it).y == -m_point[p_prev2].y)
            {
                if(addMovePoint(m_point[p_prev2],true))
                    return true;
            };
        };
    }
    else
        CCLOG("Error");
    return false;
};

void BLinePath::goToStart()
{
    curent_position_it = path_list.begin();
    curent_point = *curent_position_it;
    path_position = 0;
};

void BLinePath::load(cocos2d::CCDictionary *_dict)
{
    if(_dict)
    {
        
        cocos2d::CCPoint start = cocos2d::CCPointFromString(dictStr(_dict, "Start"));
        curent_point.set(short(start.x), short(start.y));
        CCLOG("initial::curent_point x %d y %d path %s",curent_point.x,curent_point.y,dictStr(_dict, "Name"));
        init(dictStr(_dict, "Name"));
    }
}
void BLinePath::init(const char *_filename)
{
    cocos2d::CCImage::EImageFormat imageType = cocos2d::CCImage::kFmtPng;
    cocos2d::CCImage* pimga = new cocos2d::CCImage;
    CCAssert(pimga->initWithImageFile(_filename,imageType), "error path init");
    
    unsigned char * data = pimga->getData();
    path_width = pimga->getWidth();
    path_height = pimga->getHeight();
    CCLOG("path size %d %d",path_width,path_height,pimga->getWidth());
    path_data = new unsigned char*[path_width];
    unsigned char ** tdata = new unsigned char*[path_width];
    path_length  = 0;
    for(int i = 0; i < path_width; ++i)
    {
        path_data[i] = new unsigned char[path_height];
        tdata[i] = new unsigned char[path_height];
    };
    
    int lengt = 0;
    
    for(int i = 0;i < path_height; i++)
    {
        //   printf("\n");
        for(int j = 0;j < path_width; j++)
        {
            unsigned char d = 0;
            cocos2d::ccColor4B color;
            color.r = data[(i*path_width + j)*4 + 0];
            color.g = data[(i*path_width + j)*4 + 1];
            color.b = data[(i*path_width + j)*4 + 2];
            color.a = data[(i*path_width + j)*4 + 3];
            if(color.a != 0 )
            {
                if(color.r != 0)
                {
                    d = 1;
                }
                else if(color.b != 0)
                {
                    d = fLimit;
                }
                else
                {
                    d = 255;
                }
                // printf("%d %d %d \n",color.r,color.g,color.b);
                
            }
            if(d == 0)
            {
                if(color.r !=0 ||color.b!=0 || color.g !=0 )
                    printf("%d %d %d",color.r,color.g,color.b);
            }
            
            
            
            if(d != 0)
            {
                lengt++;
                path_data[j][path_height - i - 1] = 254;
                tdata[j][path_height - i - 1] = d;
            }
            else
            {
                path_data[j][path_height - i - 1] = 0;
                tdata[j][path_height - i - 1] = 0;
            }
        };
    };
    
    
    CC_SAFE_DELETE(pimga);
    calcLength(curent_point);
    
    for(int i = 0; i < path_width; ++i)
    {
        delete[] path_data[i];
        path_data[i] = NULL;
    };
    delete[] path_data;
    path_data = NULL;
    path_data = tdata;
    tdata = NULL;
    path_list.pop_back();
    path_position = 0;
    curent_position_it = path_list.begin();
    // path_list.();
    CCLOG("path_length %d %d",path_length,lengt);
    
};
