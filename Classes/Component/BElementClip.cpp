//
//  BElementClip.cpp
//  Bra
//
//  Created by PR1.Stigol on 25.03.13.
//
//

#include "BElementClip.h"
#include "BComponent.h"

#pragma mark - CLIP ELEMENT
BElementClip::BElementClip()
{
    b_lookpoint = false;
    b_return = false;
    b_moved = false;
    is_active_path = true;
};

BElementClip::~BElementClip()
{
    CC_SAFE_DELETE(m_path);
};

BElement *BElementClip::create(void)
{
    BElement * pRet = new BElementClip();
    pRet->autorelease();
    return pRet;
};

bool BElementClip::isPointInObject(const CCPoint &_p)
{
    return pointInNode(this, _p);
};

void BElementClip::startEnd()
{
    // schedule(schedule_selector(BElementButton::moveToEnd), 0.003);
};

void BElementClip::moveToEnd()
{
    /*   if(path_position < path_length)
     {
     path_position++;
     curent_position_it++;
     curent_point += *curent_position_it;
     setPosition(ccpAdd(ccp(curent_point.x,curent_point.y),diff_point));
     }
     else
     {
     unscheduleAllSelectors();
     }*/
};

void BElementClip::moveToStart()
{
    /*  if(path_position > 1)
     {
     if(path_position > 20)
     {
     for (int i = 0; i < 20; ++i) {
     path_position--;
     curent_position_it--;
     curent_point -= *curent_position_it;
     }
     }
     else
     {
     path_position--;
     curent_position_it--;
     curent_point -= *curent_position_it;
     }
     setPosition(ccpAdd(ccp(curent_point.x,curent_point.y),diff_point));
     }
     else
     {
     unscheduleAllSelectors();
     }*/
};


bool BElementClip::initial(CCNode * _parent)
{
    // curent_point.set(short(0), short(0));
    //   CCPoint start;
    m_path = NULL;
    if(m_dict->objectForKey("Path"))
    {
        m_path = new BLinePath;
        m_path->load((CCDictionary *)m_dict->objectForKey("Path"));
    }
    else
    {
        CCAssert(true, "Error:: not key 'Path' in dictionary");
    };
    if(m_dict->objectForKey("Special"))
    {
        CCDictionary *tdict = dictDict(m_dict, "Special");
        if(tdict->objectForKey("Return"))
            b_return  = (dictInt(tdict,"Return") != 0);
        if(tdict->objectForKey("Moved"))
            b_moved  = (dictInt(tdict,"Moved") != 0);
        if(tdict->objectForKey("LookPoint"))
            b_lookpoint = (dictInt(tdict,"LookPoint") != 0);
        if(tdict->objectForKey("ActivePath"))
            is_active_path = (dictInt(tdict,"ActivePath") != 0);
    };
    CCArray *m_array =  dictArray(m_dict, "MovedElement");
    if(m_array)
    {
        CCObject* obj = NULL;
        CCARRAY_FOREACH(m_array,obj)
        {
			auto c = ((CCString *)obj)->getCString();
			auto bcom = static_cast<BComponent*>(_parent)->getChildByName(c);
			MovedClip clip;
			clip.dif = CCPointZero;
			clip.element = bcom;
            move_clip.push_back(clip);
        };
    };
    m_square = getContentSize().width*getContentSize().height;
    
    diff_point.x = 0;
    diff_point.y = 0;
    if(m_path)
        diff_point = ccpSub(getPosition(),ccp(m_path->getPosition().x,m_path->getPosition().y));
    type = BRA::elBUTTON;
    return true;
};
void BElementClip::reset()
{
    BElement::reset();
    CCLOG("BElementClip reset");
    if(m_dict->objectForKey("Special"))
    {
        CCDictionary *tdict = dictDict(m_dict, "Special");
        if(tdict->objectForKey("Return"))
            b_return  = (dictInt(tdict,"Return") != 0);
        if(tdict->objectForKey("Moved"))
            b_moved  = (dictInt(tdict,"Moved") != 0);
        if(tdict->objectForKey("LookPoint"))
            b_lookpoint = (dictInt(tdict,"LookPoint") != 0);
    };
    if(m_path)
        m_path->goToStart();
};
void BElementClip::beginTouch(const int _patam){
    // setState(1);
    is_active_touch = true;
    if(m_dict->objectForKey("Touch"))
    {
        CCDictionary * dict = dictDict(m_dict, "Touch");
        if(dict->objectForKey("activetedPath"))
        {
            BElement *element = static_cast<BComponent *>(getParent())->getChildByName(dictStr(dict, "activetedPath"));
            static_cast<BElementClip *>(element)->setActivePath(true);
        };
    };
};

void BElementClip::endTouch(const int _patam){
    
    if(m_dict->objectForKey("Touch"))
    {
        CCDictionary * dict = dictDict(m_dict, "Touch");
        if(dict->objectForKey("activetedPath"))
        {
            BElement *element = static_cast<BComponent *>(getParent())->getChildByName(dictStr(dict, "activetedPath"));
            static_cast<BElementClip *>(element)->setActivePath(false);
        };
    };
    is_active_touch = false;
    if(b_return)
    {
        mc_it = move_clip.begin();
        while (mc_it != move_clip.end()) {
            (*mc_it).element->getPosition();
            (*mc_it).dif = ccpSub((*mc_it).element->getPosition(), getPosition());
            mc_it++;
        };
        m_path->goToStart();
        setPosition(ccpAdd(ccp(m_path->getPosition().x,m_path->getPosition().y),diff_point));
        
        if(b_lookpoint)
            setRotation(-ccpToAngle(getPosition())*PI180 + 90);
        
        mc_it = move_clip.begin();
        while (mc_it != move_clip.end()) {
            (*mc_it).element->getPosition();
            (*mc_it).element->setPosition(ccpAdd(getPosition(), (*mc_it).dif));
            mc_it++;
        };
    }
    else
    {
        //   schedule(schedule_selector(BElementButton::moveToStart),1.0/60.0);
    }
};

bool BElementClip::moveBy(const CCPoint& _point)
{
    if(!is_active_path)
        return false;
    //CCPoint save_pos = getPosition();
    CCLOG("BElementClip::moveBy %f %f",_point.x,_point.y);
    
    point tmp = m_path->getPosition();
    
    m_path->moveToPoint(short(m_path->getPosition().x + _point.x),short(m_path->getPosition().y + _point.y));
    
//    point n_point(short(_point.x),short(_point.y));
//    n_point = tmp;
//    n_point-= m_path->getPosition();
//    CCPoint prev_pos = getPosition();
//    
    point n_point = tmp;
    n_point -= m_path->getPosition();
    if(m_square > 200*200)
    if(n_point.x == 0 && n_point.y == 0)
        return false;
    //touch_postion.x +=n_point.x;
    //touch_postion.y +=n_point.y;
    mc_it = move_clip.begin();
    while (mc_it != move_clip.end()) {
        (*mc_it).element->getPosition();
        (*mc_it).dif = ccpSub((*mc_it).element->getPosition(), getPosition());
        mc_it++;
    };
    CCPoint prev_pos = getPosition();
    setPosition(ccpAdd(ccp(m_path->getPosition().x,m_path->getPosition().y),diff_point));
    touch_postion = ccpAdd(touch_postion, ccpSub(getPosition(), prev_pos));
    
    mc_it = move_clip.begin();
    while (mc_it != move_clip.end()) {
        (*mc_it).element->getPosition();
        (*mc_it).element->setPosition(ccpAdd(getPosition(), (*mc_it).dif));
        mc_it++;
    };
    
    return true; //is element will be moved
};

bool BElementClip::moveTo(const CCPoint& _point)
{
    if(!b_moved)
        return false;
    
    if(b_lookpoint)
        setRotation(0);
    CCPoint p = convertToNodeSpace(_point);
//    CCPoint p = (ccpSub(convertToNodeSpace(_point),convertToNodeSpace(touch_postion)));
   // touch_postion = _point;
    p.x -= getContentSize().width/2;
    p.y -= getContentSize().height/2;
    //CCLOG("moveTo:: %f %f (%f %f)",_point.x,_point.y,p.x,p.y);

    if(b_lookpoint)
        setRotation(-ccpToAngle(getPosition())*PI180 + 90);
    
    //  CCLOG("moveTo(%f %f);  pos(%f %f) len %f",p.x,p.y,getPositionX(),getPositionY());
    return moveBy(p);
};
