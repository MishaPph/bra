//
//  BElementAngleClip.cpp
//  Bra
//
//  Created by PR1.Stigol on 25.03.13.
//
//

#include "BElementAngleClip.h"
#pragma mark - CLIP ELEMENT ANGEL
BElementAngleClip::BElementAngleClip()
{
};

BElementAngleClip::~BElementAngleClip()
{
};

BElement *BElementAngleClip::create(void)
{
    BElement * pRet = new BElementAngleClip();
    pRet->autorelease();
    return pRet;
};

bool BElementAngleClip::moveTo(const CCPoint& _point)
{
    CCPoint p = getParent()->convertToNodeSpace(_point);
    p = ccpNormalize(ccpSub(p,getPosition()));
    int angel = -CC_RADIANS_TO_DEGREES(ccpToAngle(p)) + 90;
    //CCLOG("angel %d",angel);
    if( angel > 180 && is_alig180)
    {
        angel -= 360;
    };
    if(angel > up_angle)
        angel = up_angle;
    if(angel < low_angle)
        angel = low_angle;
    
    setRotation(angel);
    return true;
};

bool BElementAngleClip::isPointInObject(const CCPoint &_p)
{
    
    CCPoint ang = ccpRotateByAngle(_p, getPosition(),CC_DEGREES_TO_RADIANS(getRotation()) );
    
    //   CCLOG("isPointInObject :: (%f %f) (%f %f)",_p.x,_p.y,ang.x,ang.y);
    
    CCPoint pLB = ccp(getPosition().x - getContentSize().width*getAnchorPoint().x,getPosition().y - getContentSize().height*getAnchorPoint().y);
    CCPoint p = ccp(ang.x - pLB.x,ang.y - pLB.y);
    // if(getRotation() != 0)
    //     p = ccpRotateByAngle(p, ccp(0,0), CC_DEGREES_TO_RADIANS(getRotation()));
    
    if(p.x > 0 && p.y >0 )
    {
        if((p.x < (getContentSize().width*getScaleX())) && (p.y < (getContentSize().height*getScaleY())))
        {
            if(has_container)
                return isPointInContainer(p);
            else
                return true;
        }
    };
    return false;
    //  return true;
}
bool BElementAngleClip::initial(CCNode * _parent)
{
    //
    low_angle = 0;
    up_angle = 0;
    if(m_dict->objectForKey("Special"))
    {
        CCDictionary *tdict = dictDict(m_dict, "Special");
        if(tdict->objectForKey("UpAngle"))
            up_angle  = dictInt(tdict,"UpAngle");
        if(tdict->objectForKey("LowAngle"))
            low_angle  = dictInt(tdict,"LowAngle");
        is_alig180 = true;
        if(tdict->objectForKey("Alig180"))
            is_alig180  = dictInt(tdict,"Alig180");
    };
    
    type = BRA::elBUTTON;
    return true;
};
void BElementAngleClip::reset()
{
    BElement::reset();
};

void BElementAngleClip::beginTouch(const int _patam){
    
    is_active_touch = true;
};

void BElementAngleClip::endTouch(const int _patam){
    
    is_active_touch = false;
};