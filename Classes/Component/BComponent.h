//
//  BComponent.h
//  Bra
//
//  Created by PR1.Stigol on 01.03.13.
//
//

#ifndef __Bra__BComponent__
#define __Bra__BComponent__
#include "cocos2d.h"
#include <iostream>
//#include "list.h"
//#include "map.h"
#include  "BRA.h"
#include "BLinePath.h"
#include "BElement.h"

class BElementClipper: public BElement
{
private:

public:
    BElementClipper();
    virtual bool isPointInObject(const CCPoint &_p);
    virtual ~BElementClipper();
    static BElement * create(void);
    virtual bool initial(CCNode * parent = NULL);
    virtual void beginTouch(const int = 0){};
    virtual void endTouch(const int = 0){};
};
//    
class BElementLabel: public CCLabelTTF
{
private:
    
public:
    BElementLabel();
    bool loadWithDict(CCDictionary *_dict);
    void setString(const std::string &_str);
    void setString(const int &_value);
    virtual ~BElementLabel();
    static BElementLabel * create(void);
	virtual bool isPointInObject(const CCPoint &_p) { return true; };
    virtual bool initial(CCNode * parent = NULL);
    virtual void beginTouch(const int = 0){};
    virtual void endTouch(const int = 0){};
};
    
class BElementPicture: public BElement
{
private:
public:
    BElementPicture();
    virtual ~BElementPicture();
    static BElement * create(void);
    virtual bool isPointInObject(const CCPoint &_p);
    virtual bool initial(CCNode * parent = NULL);
    virtual void beginTouch(const int = 0){};
    virtual void endTouch(const int = 0){};
};

class BElementCheckBox: public BElement
{
private:
    bool is_checked;
public:
    BElementCheckBox();
    virtual ~BElementCheckBox();
    void setChecked(bool _check);
    bool isChecked()
    {
        return is_checked;
    };
    virtual bool isPointInObject(const CCPoint &_p);
    virtual void beginTouch(const int = 0);
    virtual void endTouch(const int = 0);
    static BElement * create(void);
    virtual bool initial(CCNode * parent = NULL);
};
    
class BElementRadioButton: public BElementCheckBox
{
private:
    
public:
    virtual ~BElementRadioButton();
    virtual bool isPointInObject(const CCPoint &_p);
    static BElement * create(void);
    virtual bool initial(CCNode * parent = NULL);
};

class BComponent: public CCNode
{
protected:
    //std::list<BElement*>
    CCDictionary *m_dict;
    //std::list<BElement*> m_element;
    std::list<BElement*>::iterator it;
    BElement* createElementWithName(const char *_type);//factory method
    std::map<std::string, SEL_CallFuncND> func_map;
    std::vector<std::string > atlas_map;
    std::vector<std::string > atlas_texture_name;
    std::vector<CCTexture2D *> atlas_texture;
    
    std::string file_name;
    static std::list<std::string> list_name;
    std::list<std::string>::iterator it_name;
    CCObject*  m_listener;
    bool is_load;
    int glob_type;
public:
    BElement* getChildByName(const char *_name);
    void unloadAtlasTexture();
    void loadAtlasTexture();
    int getType() const{
        return glob_type;
    };
    bool isLoad()
    {
        return is_load;
    };
    void addFuncToMap(CCObject* _listen,const std::string& _name,SEL_CallFuncND _func_sel)
    {
        CCLOG("addFuncToMap:: name %s",_name.c_str());
        m_listener = _listen;
        func_map[_name] = _func_sel;
    };
    void runComplite();
    void reset();
    BComponent();
    BElement *getTouchElement(const CCPoint &_touch,int _filter);
    virtual ~BComponent();
    static BComponent * create(void);
    static BComponent * createBath(void);
    virtual void draw(void);
    bool loadWithFile(const char* _filename);
};

#endif /* defined(__Bra__BComponent__) */
