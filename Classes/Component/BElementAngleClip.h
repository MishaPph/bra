//
//  BElementAngleClip.h
//  Bra
//
//  Created by PR1.Stigol on 25.03.13.
//
//

#ifndef __Bra__BElementAngleClip__
#define __Bra__BElementAngleClip__

#include <iostream>
#include "BElement.h"

class BElementAngleClip : public BElement
{
private:
    int low_angle,up_angle;
    bool is_alig180;
public:
    BElementAngleClip();
    virtual ~BElementAngleClip();
    virtual void beginTouch(const int = 0);
    virtual void endTouch(const int = 0);
    virtual void reset();
    static BElement * create(void);
    bool moveTo(const CCPoint& _point);
    void startEnd();
    void moveToStart();
    virtual bool initial(CCNode * _parent = NULL);
    virtual bool isPointInObject(const CCPoint &_p);
};
#endif /* defined(__Bra__BElementAngleClip__) */
