//
//  BSpriteBody.cpp
//  Bra
//
//  Created by PR1.Stigol on 25.01.13.
//
//

#include "BSpriteBody.h"
std::list<BraJoinSprite *>BraJoinSprite::list_bra;

using namespace cocos2d;
#pragma  mark - STATIC_METHOS
void BraJoinSprite::updateAll()
{
    std::list<BraJoinSprite *>::iterator it = BraJoinSprite::list_bra.begin();
    while (it != BraJoinSprite::list_bra.end()) {
        (*it)->updateBody();
        it++;
    }
};

#pragma mark - INIT_METHOD

BraJoinSprite* BraJoinSprite::create()
{
    BraJoinSprite *pSprite = new BraJoinSprite();
    if (pSprite && pSprite->init())
    {
        pSprite->autorelease();
        return pSprite;
    }
    CC_SAFE_DELETE(pSprite);
    return NULL;
};

BraJoinSprite::BraJoinSprite()
{
    BraJoinSprite::list_bra.push_back(this);
    for(int i = 0; i < COUN_POINT; m_scale[i] = 1.0,m_vect2[i] = CCPointZero,i++);
};

void BraJoinSprite::initWithTwoSprite(CCSprite* _left_body,CCSprite* _right_body,const CCPoint& _o_lt,const CCPoint& _o_lb,const CCPoint& _o_rt,const cocos2d::CCPoint& _o_rb,int _type)
{
    type = _type;
    offset_lb = _o_lb;
    offset_lt = _o_lt;
    offset_rb = _o_rb;
    offset_rt = _o_rt;
    
    m_sprite[0] = _left_body;
    m_sprite[1] = _right_body;
#if DEBUG_MODE
    setVisible(false);
#endif
   // setAnchorPoint(ccp(0,0));
};

void BraJoinSprite::initWithTwoSprite(CCSprite* _left_body,CCSprite* _right_body,int _type)
{
    type = _type;    
    m_sprite[0] = _left_body;
    m_sprite[1] = _right_body;
#if DEBUG_MODE
    setVisible(false);
#endif
    setAnchorPoint(ccp(0,0));
};

void BraJoinSprite::changeWithTwoSprite(CCSprite* _left_body,CCSprite* _right_body,const CCPoint& _o_lt,const CCPoint& _o_lb,const CCPoint& _o_rt,const CCPoint& _o_rb,bool _replace)
{
    if(_replace)
    {
        offset_lb = _o_lb;
        offset_lt = _o_lt;
        offset_rb = _o_rb;
        offset_rt = _o_rt;
    }
    else
    {
        offset_lb = ccpAdd(offset_lb, _o_lb);
        offset_lt = ccpAdd(offset_lt, _o_lt);
        offset_rb = ccpAdd(offset_rb,_o_rb);
        offset_rt = ccpAdd(offset_rt,_o_rt);
    }
    m_sprite[0] = _left_body;
    m_sprite[1] = _right_body;

   // setAnchorPoint(ccp(0,0));
};


void BraJoinSprite::setScale(const float &_scale)
{
    offset_lb = offset_lb * _scale;
    offset_lt = offset_lt * _scale;
    offset_rb = offset_rb * _scale;
    offset_rt = offset_rt * _scale;
};

void BraJoinSprite::initWithPoint(const CCPoint &_tl,const CCPoint &_bl,const CCPoint &_tr,const CCPoint &_br)
{
    type = 5;
    //bottom left body

    m_sQuad.tl.vertices.x = _tl.x;
    m_sQuad.tl.vertices.y = _tl.y;

    m_sQuad.bl.vertices.x = _bl.x;
    m_sQuad.bl.vertices.y = _bl.y;
    
    m_sQuad.tr.vertices.x = _tr.x;
    m_sQuad.tr.vertices.y = _tr.y;
    
    m_sQuad.br.vertices.x = _br.x;
    m_sQuad.br.vertices.y = _br.y;
    
};

#pragma mark - OPERATION


void BraJoinSprite::updateBody()
{

    if(type == 1)
    {

    }
    else if(type == 2)
    {

    }
    else if(type == 3)
        {
        
        }
    else if(type == 4)
    {

    }else if(type == 5)
    {
        
    }else if(type == 6)
    {
        float angle = m_sprite[0]->getRotation();
        float x1 = cos(angle)*offset_lt.x - sin(angle)*offset_lt.y;
        float y1 = sin(angle)*offset_lt.x + cos(angle)*offset_lt.y;
        
        CCPoint left = m_sprite[0]->getPosition();
        CCPoint right = convertToNodeSpace(m_sprite[1]->getParent()->convertToWorldSpace(m_sprite[1]->getPosition()));
        //bottom left body
        m_sQuad.tl.vertices.x = (left.x + x1)*m_scale[0];
        m_sQuad.tl.vertices.y = (left.y + y1)*m_scale[0];
        
        x1 = cos(angle)*offset_lb.x - sin(angle)*offset_lb.y;
        y1 = sin(angle)*offset_lb.x + cos(angle)*offset_lb.y;
        //top left body
        m_sQuad.bl.vertices.x = (left.x + x1)*m_scale[0];
        m_sQuad.bl.vertices.y = (left.y + y1)*m_scale[0];
        
        angle = m_sprite[1]->getRotation();
        //top right body
        x1 = cos(angle)*offset_rb.x - sin(angle)*offset_rb.y;
        y1 = sin(angle)*offset_rb.x + cos(angle)*offset_rb.y;
        m_sQuad.br.vertices.x = (right.x + x1)*m_scale[1];
        m_sQuad.br.vertices.y = (right.y+ y1)*m_scale[1];
        
        //bottom right bod
        x1 = cos(angle)*offset_rt.x - sin(angle)*offset_rt.y;
        y1 = sin(angle)*offset_rt.x + cos(angle)*offset_rt.y;
        m_sQuad.tr.vertices.x = (right.x + x1)*m_scale[1];
        m_sQuad.tr.vertices.y = (right.y + y1)*m_scale[1];
    }
    else if(type == 7)
    {

    }
    else
    {
      };
    setDirty(true);
};

