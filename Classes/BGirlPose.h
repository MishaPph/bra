//
//  BGirlPose.h
//  Bra
//
//  Created by PR1.Stigol on 21.03.13.
//
//

#ifndef __Bra__BGirlPose__
#define __Bra__BGirlPose__
#include "Component\BComponent.h"

#include <iostream>
class BGirlPose
{
public:
    int num;
    int type_pose;
    BComponent *m_pose;
    BElement *girl;
    BElement *hair;
    BElement *bikini;
    cocos2d::CCSize win_size;
    int hair_num;
    int girl_num;
    int bikini_num;
    BGirlPose();
    ~BGirlPose();
    void setType(const int& _type);
    void randHair();
    void randGirl();
    void animGirlFrame(const int &_frame);
    void randBikini();
    void create(int _pose);
};

#endif /* defined(__Bra__BGirlPose__) */
