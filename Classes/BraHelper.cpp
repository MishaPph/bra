//
//  BraHelper.cpp
//  Bra
//
//  Created by PR1.Stigol on 29.01.13.
//
//

#include "BraHelper.h"
static BraHelper* m_bra_helper = NULL;
BraHelper* BraHelper::Instance()
{
    if(!m_bra_helper)
    {
        m_bra_helper = new BraHelper();
    }
    return m_bra_helper;
};